var agentVersioning = {
  "contractName": "AgentVersioning",
  "abi": [
    {
      "inputs": [
        {
          "name": "management",
          "type": "address"
        },
        {
          "name": "_staking",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "host",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "agent",
          "type": "string"
        }
      ],
      "name": "ChangedAgentVersion",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "host",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "agent",
          "type": "string"
        }
      ],
      "name": "ClearedAgent",
      "type": "event"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_newOwner",
          "type": "address"
        }
      ],
      "name": "changeOwner",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_management",
          "type": "address"
        }
      ],
      "name": "setManagement",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_staking",
          "type": "address"
        }
      ],
      "name": "setStaking",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_agent",
          "type": "string"
        }
      ],
      "name": "updateAgentVersion",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_agent",
          "type": "string"
        }
      ],
      "name": "clearAgent",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "host",
          "type": "address"
        },
        {
          "name": "agent",
          "type": "string"
        }
      ],
      "name": "getHostAgentVersion",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    }
  ],
  "bytecode": "0x608060405234801561001057600080fd5b50604051604080610c5383398101604052805160209091015160018054600160a060020a0319908116331790915560028054600160a060020a0394851690831617905560038054939092169216919091179055610be1806100726000396000f3006080604052600436106100775763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416634292396c811461007c578063581993c8146100f55780638ff3909914610150578063a5e8a65614610171578063a6f9dae1146101ca578063d4a22bde146101eb575b600080fd5b34801561008857600080fd5b5060408051602060046024803582810135601f81018590048502860185019096528585526100e3958335600160a060020a031695369560449491939091019190819084018382808284375094975061020c9650505050505050565b60408051918252519081900360200190f35b34801561010157600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e94369492936024939284019190819084018382808284375094975061028a9650505050505050565b005b34801561015c57600080fd5b5061014e600160a060020a036004351661048a565b34801561017d57600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e9436949293602493928401919081908401838280828437509497506105a19650505050505050565b3480156101d657600080fd5b5061014e600160a060020a0360043516610a33565b3480156101f757600080fd5b5061014e600160a060020a0360043516610ac4565b600160a060020a038216600090815260208181526040808320905184519192859282918401908083835b602083106102555780518252601f199092019160209182019101610236565b51815160209384036101000a600019018019909216911617905292019485525060405193849003019092205495945050505050565b600354604080517fc1f159700000000000000000000000000000000000000000000000000000000081523360048201529051600160a060020a039092169163c1f15970916024808201926020929091908290030181600087803b1580156102f057600080fd5b505af1158015610304573d6000803e3d6000fd5b505050506040513d602081101561031a57600080fd5b50511515600114610375576040805160e560020a62461bcd02815260206004820181905260248201527f5468652073656e64657220646f6573206e6f7420686176652061207374616b65604482015290519081900360640190fd5b33600090815260208181526040808320905184519192859282918401908083835b602083106103b55780518252601f199092019160209182019101610396565b51815160209384036101000a6000190180199092169116179052920194855250604080519485900382018520959095553380855284820186815287519686019690965286517fe435d13567ff4c27a91011dcc4f13f07d291b9218b82294c23562819a795ac3b9691958895509350909160608401919085019080838360005b8381101561044c578181015183820152602001610434565b50505050905090810190601f1680156104795780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150565b600154600160a060020a031633146104ec576040805160e560020a62461bcd02815260206004820152601b60248201527f5468652073656e646572206973206e6f7420746865206f776e65720000000000604482015290519081900360640190fd5b600160a060020a0381161515610572576040805160e560020a62461bcd02815260206004820152602160248201527f546865207374616b696e67206164647265737320706172616d206973206e756c60448201527f6c00000000000000000000000000000000000000000000000000000000000000606482015290519081900360840190fd5b6003805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600354604080517fc1f15970000000000000000000000000000000000000000000000000000000008152336004820152905160009283928392600160a060020a039092169163c1f159709160248082019260209290919082900301818787803b15801561060d57600080fd5b505af1158015610621573d6000803e3d6000fd5b505050506040513d602081101561063757600080fd5b50511515600114610692576040805160e560020a62461bcd02815260206004820181905260248201527f5468652073656e64657220646f6573206e6f7420686176652061207374616b65604482015290519081900360640190fd5b6002546040517ffd4eb5b0000000000000000000000000000000000000000000000000000000008152602060048201818152875160248401528751600160a060020a039094169363fd4eb5b093899383926044909201919085019080838360005b8381101561070b5781810151838201526020016106f3565b50505050905090810190601f1680156107385780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561075757600080fd5b505af115801561076b573d6000803e3d6000fd5b505050506040513d602081101561078157600080fd5b50516002546040517f13910305000000000000000000000000000000000000000000000000000000008152602060048201818152885160248401528851949750600160a060020a03909316936313910305938993909283926044909201919085019080838360005b838110156108015781810151838201526020016107e9565b50505050905090810190601f16801561082e5780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561084d57600080fd5b505af1158015610861573d6000803e3d6000fd5b505050506040513d602081101561087757600080fd5b5051600254604080517fc09ab91a000000000000000000000000000000000000000000000000000000008152600160a060020a03878116600483015260248201859052915193955091169163c09ab91a916044808201926020929091908290030181600087803b1580156108ea57600080fd5b505af11580156108fe573d6000803e3d6000fd5b505050506040513d602081101561091457600080fd5b5051336000908152602081815260409182902091518751939450849388928291908401908083835b6020831061095b5780518252601f19909201916020918201910161093c565b51815160209384036101000a600019018019909216911617905292019485525060408051948590038201852095909555338085528482018681528a519686019690965289517f947de2d26b84253829de965a6c4aa2608a54ec2e10506166f2a27c76538bd8659691958b95509350909160608401919085019080838360005b838110156109f25781810151838201526020016109da565b50505050905090810190601f168015610a1f5780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150505050565b600154600160a060020a03163314610a95576040805160e560020a62461bcd02815260206004820152601b60248201527f5468652073656e646572206973206e6f7420746865206f776e65720000000000604482015290519081900360640190fd5b6001805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600154600160a060020a03163314610b26576040805160e560020a62461bcd02815260206004820152601b60248201527f5468652073656e646572206973206e6f7420746865206f776e65720000000000604482015290519081900360640190fd5b600160a060020a0381161515610b86576040805160e560020a62461bcd02815260206004820152601c60248201527f546865206d616e6167656d656e7420706172616d206973206e756c6c00000000604482015290519081900360640190fd5b6002805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03929092169190911790555600a165627a7a7230582024c06a088313106cfcfda89cd0a883b45440abf451fec15c15eb6fd0ec8ccf100029",
  "deployedBytecode": "0x6080604052600436106100775763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416634292396c811461007c578063581993c8146100f55780638ff3909914610150578063a5e8a65614610171578063a6f9dae1146101ca578063d4a22bde146101eb575b600080fd5b34801561008857600080fd5b5060408051602060046024803582810135601f81018590048502860185019096528585526100e3958335600160a060020a031695369560449491939091019190819084018382808284375094975061020c9650505050505050565b60408051918252519081900360200190f35b34801561010157600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e94369492936024939284019190819084018382808284375094975061028a9650505050505050565b005b34801561015c57600080fd5b5061014e600160a060020a036004351661048a565b34801561017d57600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261014e9436949293602493928401919081908401838280828437509497506105a19650505050505050565b3480156101d657600080fd5b5061014e600160a060020a0360043516610a33565b3480156101f757600080fd5b5061014e600160a060020a0360043516610ac4565b600160a060020a038216600090815260208181526040808320905184519192859282918401908083835b602083106102555780518252601f199092019160209182019101610236565b51815160209384036101000a600019018019909216911617905292019485525060405193849003019092205495945050505050565b600354604080517fc1f159700000000000000000000000000000000000000000000000000000000081523360048201529051600160a060020a039092169163c1f15970916024808201926020929091908290030181600087803b1580156102f057600080fd5b505af1158015610304573d6000803e3d6000fd5b505050506040513d602081101561031a57600080fd5b50511515600114610375576040805160e560020a62461bcd02815260206004820181905260248201527f5468652073656e64657220646f6573206e6f7420686176652061207374616b65604482015290519081900360640190fd5b33600090815260208181526040808320905184519192859282918401908083835b602083106103b55780518252601f199092019160209182019101610396565b51815160209384036101000a6000190180199092169116179052920194855250604080519485900382018520959095553380855284820186815287519686019690965286517fe435d13567ff4c27a91011dcc4f13f07d291b9218b82294c23562819a795ac3b9691958895509350909160608401919085019080838360005b8381101561044c578181015183820152602001610434565b50505050905090810190601f1680156104795780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150565b600154600160a060020a031633146104ec576040805160e560020a62461bcd02815260206004820152601b60248201527f5468652073656e646572206973206e6f7420746865206f776e65720000000000604482015290519081900360640190fd5b600160a060020a0381161515610572576040805160e560020a62461bcd02815260206004820152602160248201527f546865207374616b696e67206164647265737320706172616d206973206e756c60448201527f6c00000000000000000000000000000000000000000000000000000000000000606482015290519081900360840190fd5b6003805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600354604080517fc1f15970000000000000000000000000000000000000000000000000000000008152336004820152905160009283928392600160a060020a039092169163c1f159709160248082019260209290919082900301818787803b15801561060d57600080fd5b505af1158015610621573d6000803e3d6000fd5b505050506040513d602081101561063757600080fd5b50511515600114610692576040805160e560020a62461bcd02815260206004820181905260248201527f5468652073656e64657220646f6573206e6f7420686176652061207374616b65604482015290519081900360640190fd5b6002546040517ffd4eb5b0000000000000000000000000000000000000000000000000000000008152602060048201818152875160248401528751600160a060020a039094169363fd4eb5b093899383926044909201919085019080838360005b8381101561070b5781810151838201526020016106f3565b50505050905090810190601f1680156107385780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561075757600080fd5b505af115801561076b573d6000803e3d6000fd5b505050506040513d602081101561078157600080fd5b50516002546040517f13910305000000000000000000000000000000000000000000000000000000008152602060048201818152885160248401528851949750600160a060020a03909316936313910305938993909283926044909201919085019080838360005b838110156108015781810151838201526020016107e9565b50505050905090810190601f16801561082e5780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b15801561084d57600080fd5b505af1158015610861573d6000803e3d6000fd5b505050506040513d602081101561087757600080fd5b5051600254604080517fc09ab91a000000000000000000000000000000000000000000000000000000008152600160a060020a03878116600483015260248201859052915193955091169163c09ab91a916044808201926020929091908290030181600087803b1580156108ea57600080fd5b505af11580156108fe573d6000803e3d6000fd5b505050506040513d602081101561091457600080fd5b5051336000908152602081815260409182902091518751939450849388928291908401908083835b6020831061095b5780518252601f19909201916020918201910161093c565b51815160209384036101000a600019018019909216911617905292019485525060408051948590038201852095909555338085528482018681528a519686019690965289517f947de2d26b84253829de965a6c4aa2608a54ec2e10506166f2a27c76538bd8659691958b95509350909160608401919085019080838360005b838110156109f25781810151838201526020016109da565b50505050905090810190601f168015610a1f5780820380516001836020036101000a031916815260200191505b50935050505060405180910390a150505050565b600154600160a060020a03163314610a95576040805160e560020a62461bcd02815260206004820152601b60248201527f5468652073656e646572206973206e6f7420746865206f776e65720000000000604482015290519081900360640190fd5b6001805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b600154600160a060020a03163314610b26576040805160e560020a62461bcd02815260206004820152601b60248201527f5468652073656e646572206973206e6f7420746865206f776e65720000000000604482015290519081900360640190fd5b600160a060020a0381161515610b86576040805160e560020a62461bcd02815260206004820152601c60248201527f546865206d616e6167656d656e7420706172616d206973206e756c6c00000000604482015290519081900360640190fd5b6002805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03929092169190911790555600a165627a7a7230582024c06a088313106cfcfda89cd0a883b45440abf451fec15c15eb6fd0ec8ccf100029",
  "sourceMap": "204:1823:6:-;;;643:169;8:9:-1;5:2;;;30:1;27;20:12;5:2;643:169:6;;;;;;;;;;;;;;;;;;;700:5;:18;;-1:-1:-1;;;;;;700:18:6;;;708:10;700:18;;;;725:15;:46;;-1:-1:-1;;;;;725:46:6;;;;;;;;;778:7;:28;;;;;;;;;;;;;;204:1823;;;;;;",
  "deployedSourceMap": "204:1823:6:-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1891:133;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;1891:133:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;1891:133:6;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;1891:133:6;;-1:-1:-1;1891:133:6;;-1:-1:-1;;;;;;;1891:133:6;;;;;;;;;;;;;;;;;1705:151;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;1705:151:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;1705:151:6;;-1:-1:-1;1705:151:6;;-1:-1:-1;;;;;;;1705:151:6;;;1098:171;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;1098:171:6;-1:-1:-1;;;;;1098:171:6;;;;;1291:392;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;1291:392:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;1291:392:6;;-1:-1:-1;1291:392:6;;-1:-1:-1;;;;;;;1291:392:6;816:80;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;816:80:6;-1:-1:-1;;;;;816:80:6;;;;;900:194;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;900:194:6;-1:-1:-1;;;;;900:194:6;;;;;1891:133;-1:-1:-1;;;;;1992:19:6;;1969:7;1992:19;;;;;;;;;;;:26;;;;:19;;2012:5;;1992:26;;;;;;;;36:153:-1;66:2;58:11;;36:153;;176:10;;164:23;;-1:-1;;139:12;;;;98:2;89:12;;;;114;36:153;;;299:10;344;;263:2;259:12;;;254:3;250:22;-1:-1;;246:30;311:9;;295:26;;;340:21;;377:20;365:33;;1992:26:6;;;;;-1:-1:-1;1992:26:6;;;;;;;;;;;;1891:133;-1:-1:-1;;;;;1891:133:6:o;1705:151::-;435:7;;:40;;;;;;464:10;435:40;;;;;;-1:-1:-1;;;;;435:7:6;;;;:28;;:40;;;;;;;;;;;;;;;:7;;:40;;;5:2:-1;;;;30:1;27;20:12;5:2;435:40:6;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;435:40:6;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;435:40:6;:48;;479:4;435:48;427:93;;;;;-1:-1:-1;;;;;427:93:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1783:10;1805:1;1769:25;;;;;;;;;;;:33;;;;:25;;1795:6;;1769:33;;;;;;;;36:153:-1;66:2;58:11;;36:153;;176:10;;164:23;;-1:-1;;139:12;;;;98:2;89:12;;;;114;36:153;;;299:10;344;;263:2;259:12;;;254:3;250:22;-1:-1;;246:30;311:9;;295:26;;;340:21;;377:20;365:33;;1769::6;;;;;-1:-1:-1;1769:33:6;;;;;;;;;;;:37;;;;1831:10;1818:32;;;;;;;;;;;;;;;;;;;;;;1831:10;;1818:32;;-1:-1:-1;1769:33:6;-1:-1:-1;1818:32:6;;;;;;;;;;;;;-1:-1:-1;8:100;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1818:32:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1705:151;:::o;1098:171::-;588:5;;-1:-1:-1;;;;;588:5:6;574:10;:19;566:59;;;;;-1:-1:-1;;;;;566:59:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;1168:22:6;;;;1160:68;;;;;-1:-1:-1;;;;;1160:68:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1235:7;:28;;-1:-1:-1;;1235:28:6;-1:-1:-1;;;;;1235:28:6;;;;;;;;;;1098:171::o;1291:392::-;435:7;;:40;;;;;;464:10;435:40;;;;;;1363:20;;;;;;-1:-1:-1;;;;;435:7:6;;;;:28;;:40;;;;;;;;;;;;;;;1363:20;435:7;:40;;;5:2:-1;;;;30:1;27;20:12;5:2;435:40:6;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;435:40:6;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;435:40:6;:48;;479:4;435:48;427:93;;;;;-1:-1:-1;;;;;427:93:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1386:15;;:42;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;1386:15:6;;;;:34;;1421:6;;1386:42;;;;;;;;;;;;;;:15;8:100:-1;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1386:42:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1386:42:6;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1386:42:6;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1386:42:6;1453:15;;:43;;;;;1386:42;1453:43;;;;;;;;;;;;;;1386:42;;-1:-1:-1;;;;;;1453:15:6;;;;:35;;1489:6;;1453:43;;;;;;;;;;;;;;;;:15;8:100:-1;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1453:43:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1453:43:6;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1453:43:6;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1453:43:6;1521:15;;:55;;;;;;-1:-1:-1;;;;;1521:55:6;;;;;;;;;;;;;;;1453:43;;-1:-1:-1;1521:15:6;;;:31;;:55;;;;;1453:43;;1521:55;;;;;;;;:15;;:55;;;5:2:-1;;;;30:1;27;20:12;5:2;1521:55:6;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1521:55:6;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1521:55:6;1597:10;1583:13;:25;;;1521:55;1583:25;;;;;;;;:33;;;;1521:55;;-1:-1:-1;1521:55:6;;1609:6;;1583:33;;;;;;;;;36:153:-1;66:2;58:11;;36:153;;176:10;;164:23;;-1:-1;;139:12;;;;98:2;89:12;;;;114;36:153;;;299:10;344;;263:2;259:12;;;254:3;250:22;-1:-1;;246:30;311:9;;295:26;;;340:21;;377:20;365:33;;1583::6;;;;;-1:-1:-1;1583:33:6;;;;;;;;;;;:43;;;;1658:10;1638:39;;;;;;;;;;;;;;;;;;;;;;1658:10;;1638:39;;-1:-1:-1;1583:33:6;-1:-1:-1;1638:39:6;;;;;;;;;;;;;-1:-1:-1;8:100;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;1638:39:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1291:392;;;;:::o;816:80::-;588:5;;-1:-1:-1;;;;;588:5:6;574:10;:19;566:59;;;;;-1:-1:-1;;;;;566:59:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;873:5;:17;;-1:-1:-1;;873:17:6;-1:-1:-1;;;;;873:17:6;;;;;;;;;;816:80::o;900:194::-;588:5;;-1:-1:-1;;;;;588:5:6;574:10;:19;566:59;;;;;-1:-1:-1;;;;;566:59:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;976:25:6;;;;968:66;;;;;-1:-1:-1;;;;;968:66:6;;;;;;;;;;;;;;;;;;;;;;;;;;;;1041:15;:47;;-1:-1:-1;;1041:47:6;-1:-1:-1;;;;;1041:47:6;;;;;;;;;;900:194::o",
  "source": "pragma solidity 0.4.24;\n\nimport \"contracts/interfaces/IAgentManagement.sol\";\nimport \"contracts/interfaces/IStaking.sol\";\nimport \"contracts/interfaces/IAgentVersioning.sol\";\n\n//NEW: changed contract name\n\ncontract AgentVersioning is IAgentVersioning {\n\n  mapping(address => mapping(string => uint256)) agentVersions;\n\n  address owner;\n\n  IAgentManagement agentManagement;\n  IStaking staking;\n\n  modifier onlyStakedHost() {\n\n    require(staking.getIsCurrentlyStaked(msg.sender) == true, \"The sender does not have a stake\");\n\n    _;\n\n  }\n\n  modifier onlyOwner() {\n\n    require(msg.sender == owner, \"The sender is not the owner\");\n\n    _;\n\n  }\n\n  constructor(address management, address _staking) {\n\n    owner = msg.sender;\n\n    agentManagement = IAgentManagement(management);\n\n    staking = IStaking(_staking);\n\n  }\n\n  function changeOwner(address _newOwner) onlyOwner {\n\n    owner = _newOwner;\n\n  }\n\n  function setManagement(address _management) public onlyOwner {\n\n    require(_management != address(0), \"The management param is null\");\n\n    agentManagement = IAgentManagement(_management);\n\n  }\n\n  function setStaking(address _staking) public onlyOwner {\n\n    require(_staking != address(0), \"The staking address param is null\");\n\n    staking = IStaking(_staking);\n\n  }\n\n  //Changed name\n\n  function updateAgentVersion(string _agent) public onlyStakedHost {\n\n    address agentCreator = agentManagement.getCreatorFromName(_agent);\n    uint256 position = agentManagement.getPositionFromName(_agent);\n\n    uint256 version = agentManagement.getAgentVersion(agentCreator, position);\n\n    agentVersions[msg.sender][_agent] = version;\n\n    emit ChangedAgentVersion(msg.sender, _agent);\n\n  }\n\n  //Changed name\n\n  function clearAgent(string _agent) public onlyStakedHost {\n\n    agentVersions[msg.sender][_agent] = 0;\n\n    emit ClearedAgent(msg.sender, _agent);\n\n  }\n\n  //GETTERS\n\n  //Changed name\n\n  function getHostAgentVersion(address host, string agent) public view returns (uint256) {\n\n    return agentVersions[host][agent];\n\n  }\n\n}\n",
  "sourcePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/hosts/AgentVersioning.sol",
  "ast": {
    "absolutePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/hosts/AgentVersioning.sol",
    "exportedSymbols": {
      "AgentVersioning": [
        4864
      ]
    },
    "id": 4865,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 4652,
        "literals": [
          "solidity",
          "0.4",
          ".24"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:23:6"
      },
      {
        "absolutePath": "contracts/interfaces/IAgentManagement.sol",
        "file": "contracts/interfaces/IAgentManagement.sol",
        "id": 4653,
        "nodeType": "ImportDirective",
        "scope": 4865,
        "sourceUnit": 10641,
        "src": "25:51:6",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IStaking.sol",
        "file": "contracts/interfaces/IStaking.sol",
        "id": 4654,
        "nodeType": "ImportDirective",
        "scope": 4865,
        "sourceUnit": 12004,
        "src": "77:43:6",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IAgentVersioning.sol",
        "file": "contracts/interfaces/IAgentVersioning.sol",
        "id": 4655,
        "nodeType": "ImportDirective",
        "scope": 4865,
        "sourceUnit": 10685,
        "src": "121:51:6",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 4656,
              "name": "IAgentVersioning",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10684,
              "src": "232:16:6",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentVersioning_$10684",
                "typeString": "contract IAgentVersioning"
              }
            },
            "id": 4657,
            "nodeType": "InheritanceSpecifier",
            "src": "232:16:6"
          }
        ],
        "contractDependencies": [
          10684
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 4864,
        "linearizedBaseContracts": [
          4864,
          10684
        ],
        "name": "AgentVersioning",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "constant": false,
            "id": 4663,
            "name": "agentVersions",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "254:60:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
              "typeString": "mapping(address => mapping(string => uint256))"
            },
            "typeName": {
              "id": 4662,
              "keyType": {
                "id": 4658,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "262:7:6",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "254:46:6",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                "typeString": "mapping(address => mapping(string => uint256))"
              },
              "valueType": {
                "id": 4661,
                "keyType": {
                  "id": 4659,
                  "name": "string",
                  "nodeType": "ElementaryTypeName",
                  "src": "281:6:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_storage_ptr",
                    "typeString": "string"
                  }
                },
                "nodeType": "Mapping",
                "src": "273:26:6",
                "typeDescriptions": {
                  "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                  "typeString": "mapping(string => uint256)"
                },
                "valueType": {
                  "id": 4660,
                  "name": "uint256",
                  "nodeType": "ElementaryTypeName",
                  "src": "291:7:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 4665,
            "name": "owner",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "319:13:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_address",
              "typeString": "address"
            },
            "typeName": {
              "id": 4664,
              "name": "address",
              "nodeType": "ElementaryTypeName",
              "src": "319:7:6",
              "typeDescriptions": {
                "typeIdentifier": "t_address",
                "typeString": "address"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 4667,
            "name": "agentManagement",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "337:32:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IAgentManagement_$10640",
              "typeString": "contract IAgentManagement"
            },
            "typeName": {
              "contractScope": null,
              "id": 4666,
              "name": "IAgentManagement",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10640,
              "src": "337:16:6",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                "typeString": "contract IAgentManagement"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 4669,
            "name": "staking",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "373:16:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IStaking_$12003",
              "typeString": "contract IStaking"
            },
            "typeName": {
              "contractScope": null,
              "id": 4668,
              "name": "IStaking",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 12003,
              "src": "373:8:6",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IStaking_$12003",
                "typeString": "contract IStaking"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 4683,
              "nodeType": "Block",
              "src": "420:114:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 4678,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 4674,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 12868,
                                "src": "464:3:6",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 4675,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "464:10:6",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "expression": {
                              "argumentTypes": null,
                              "id": 4672,
                              "name": "staking",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 4669,
                              "src": "435:7:6",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_IStaking_$12003",
                                "typeString": "contract IStaking"
                              }
                            },
                            "id": 4673,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "getIsCurrentlyStaked",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 11978,
                            "src": "435:28:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_external_view$_t_address_$returns$_t_bool_$",
                              "typeString": "function (address) view external returns (bool)"
                            }
                          },
                          "id": 4676,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "435:40:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 4677,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "479:4:6",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "435:48:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652073656e64657220646f6573206e6f7420686176652061207374616b65",
                        "id": 4679,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "485:34:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_1c989b5bdcc741547254981a1d4be4e836c3a58c33d76e8e4107321beb7cee3e",
                          "typeString": "literal_string \"The sender does not have a stake\""
                        },
                        "value": "The sender does not have a stake"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_1c989b5bdcc741547254981a1d4be4e836c3a58c33d76e8e4107321beb7cee3e",
                          "typeString": "literal_string \"The sender does not have a stake\""
                        }
                      ],
                      "id": 4671,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "427:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4680,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "427:93:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4681,
                  "nodeType": "ExpressionStatement",
                  "src": "427:93:6"
                },
                {
                  "id": 4682,
                  "nodeType": "PlaceholderStatement",
                  "src": "527:1:6"
                }
              ]
            },
            "documentation": null,
            "id": 4684,
            "name": "onlyStakedHost",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 4670,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "417:2:6"
            },
            "src": "394:140:6",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 4695,
              "nodeType": "Block",
              "src": "559:80:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 4690,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 4687,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12868,
                            "src": "574:3:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 4688,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "574:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "id": 4689,
                          "name": "owner",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4665,
                          "src": "588:5:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "574:19:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652073656e646572206973206e6f7420746865206f776e6572",
                        "id": 4691,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "595:29:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_8d0db2b1ac9d2dc378f870c93c1be80e6480134cdf13829b212cb3e504e73504",
                          "typeString": "literal_string \"The sender is not the owner\""
                        },
                        "value": "The sender is not the owner"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_8d0db2b1ac9d2dc378f870c93c1be80e6480134cdf13829b212cb3e504e73504",
                          "typeString": "literal_string \"The sender is not the owner\""
                        }
                      ],
                      "id": 4686,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "566:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4692,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "566:59:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4693,
                  "nodeType": "ExpressionStatement",
                  "src": "566:59:6"
                },
                {
                  "id": 4694,
                  "nodeType": "PlaceholderStatement",
                  "src": "632:1:6"
                }
              ]
            },
            "documentation": null,
            "id": 4696,
            "name": "onlyOwner",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 4685,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "556:2:6"
            },
            "src": "538:101:6",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 4720,
              "nodeType": "Block",
              "src": "693:119:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4706,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4703,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4665,
                      "src": "700:5:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "id": 4704,
                        "name": "msg",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12868,
                        "src": "708:3:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_magic_message",
                          "typeString": "msg"
                        }
                      },
                      "id": 4705,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "sender",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "708:10:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "700:18:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 4707,
                  "nodeType": "ExpressionStatement",
                  "src": "700:18:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4712,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4708,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4667,
                      "src": "725:15:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4710,
                          "name": "management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4698,
                          "src": "760:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4709,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10640,
                        "src": "743:16:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10640_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 4711,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "743:28:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "725:46:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 4713,
                  "nodeType": "ExpressionStatement",
                  "src": "725:46:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4718,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4714,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4669,
                      "src": "778:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4716,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4700,
                          "src": "797:8:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4715,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12003,
                        "src": "788:8:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$12003_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 4717,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "788:18:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "778:28:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$12003",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 4719,
                  "nodeType": "ExpressionStatement",
                  "src": "778:28:6"
                }
              ]
            },
            "documentation": null,
            "id": 4721,
            "implemented": true,
            "isConstructor": true,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4701,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4698,
                  "name": "management",
                  "nodeType": "VariableDeclaration",
                  "scope": 4721,
                  "src": "655:18:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4697,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "655:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 4700,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 4721,
                  "src": "675:16:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4699,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "675:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "654:38:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4702,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "693:0:6"
            },
            "scope": 4864,
            "src": "643:169:6",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4732,
              "nodeType": "Block",
              "src": "866:30:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4730,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4728,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4665,
                      "src": "873:5:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 4729,
                      "name": "_newOwner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4723,
                      "src": "881:9:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "873:17:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 4731,
                  "nodeType": "ExpressionStatement",
                  "src": "873:17:6"
                }
              ]
            },
            "documentation": null,
            "id": 4733,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4726,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4725,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4696,
                  "src": "856:9:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "856:9:6"
              }
            ],
            "name": "changeOwner",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4724,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4723,
                  "name": "_newOwner",
                  "nodeType": "VariableDeclaration",
                  "scope": 4733,
                  "src": "837:17:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4722,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "837:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "836:19:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4727,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "866:0:6"
            },
            "scope": 4864,
            "src": "816:80:6",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4755,
              "nodeType": "Block",
              "src": "961:133:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 4745,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 4741,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4735,
                          "src": "976:11:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 4743,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "999:1:6",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 4742,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "991:7:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 4744,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "991:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "976:25:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "546865206d616e6167656d656e7420706172616d206973206e756c6c",
                        "id": 4746,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1003:30:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_b76c630f75a5f51b4f5c44c4e1b24521c7d74b12ec894b4e7082c4c230dd3d46",
                          "typeString": "literal_string \"The management param is null\""
                        },
                        "value": "The management param is null"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_b76c630f75a5f51b4f5c44c4e1b24521c7d74b12ec894b4e7082c4c230dd3d46",
                          "typeString": "literal_string \"The management param is null\""
                        }
                      ],
                      "id": 4740,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "968:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4747,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "968:66:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4748,
                  "nodeType": "ExpressionStatement",
                  "src": "968:66:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4753,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4749,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4667,
                      "src": "1041:15:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4751,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4735,
                          "src": "1076:11:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4750,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10640,
                        "src": "1059:16:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10640_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 4752,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1059:29:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "1041:47:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 4754,
                  "nodeType": "ExpressionStatement",
                  "src": "1041:47:6"
                }
              ]
            },
            "documentation": null,
            "id": 4756,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4738,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4737,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4696,
                  "src": "951:9:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "951:9:6"
              }
            ],
            "name": "setManagement",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4736,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4735,
                  "name": "_management",
                  "nodeType": "VariableDeclaration",
                  "scope": 4756,
                  "src": "923:19:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4734,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "923:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "922:21:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4739,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "961:0:6"
            },
            "scope": 4864,
            "src": "900:194:6",
            "stateMutability": "nonpayable",
            "superFunction": 10659,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4778,
              "nodeType": "Block",
              "src": "1153:116:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 4768,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 4764,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4758,
                          "src": "1168:8:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 4766,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "1188:1:6",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 4765,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "1180:7:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 4767,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "1180:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "1168:22:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "546865207374616b696e67206164647265737320706172616d206973206e756c6c",
                        "id": 4769,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1192:35:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_bbb348a445a9181fd72bec99bddf455bf37630cdf960961a020d9caee222fc64",
                          "typeString": "literal_string \"The staking address param is null\""
                        },
                        "value": "The staking address param is null"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_bbb348a445a9181fd72bec99bddf455bf37630cdf960961a020d9caee222fc64",
                          "typeString": "literal_string \"The staking address param is null\""
                        }
                      ],
                      "id": 4763,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "1160:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4770,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1160:68:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4771,
                  "nodeType": "ExpressionStatement",
                  "src": "1160:68:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4776,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4772,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4669,
                      "src": "1235:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4774,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4758,
                          "src": "1254:8:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4773,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12003,
                        "src": "1245:8:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$12003_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 4775,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1245:18:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "1235:28:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$12003",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 4777,
                  "nodeType": "ExpressionStatement",
                  "src": "1235:28:6"
                }
              ]
            },
            "documentation": null,
            "id": 4779,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4761,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4760,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4696,
                  "src": "1143:9:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1143:9:6"
              }
            ],
            "name": "setStaking",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4759,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4758,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 4779,
                  "src": "1118:16:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4757,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1118:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1117:18:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4762,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1153:0:6"
            },
            "scope": 4864,
            "src": "1098:171:6",
            "stateMutability": "nonpayable",
            "superFunction": 10664,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4823,
              "nodeType": "Block",
              "src": "1356:327:6",
              "statements": [
                {
                  "assignments": [
                    4787
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 4787,
                      "name": "agentCreator",
                      "nodeType": "VariableDeclaration",
                      "scope": 4824,
                      "src": "1363:20:6",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 4786,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "1363:7:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 4792,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 4790,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1421:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 4788,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4667,
                        "src": "1386:15:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 4789,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getCreatorFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10562,
                      "src": "1386:34:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_address_$",
                        "typeString": "function (string memory) view external returns (address)"
                      }
                    },
                    "id": 4791,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1386:42:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1363:65:6"
                },
                {
                  "assignments": [
                    4794
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 4794,
                      "name": "position",
                      "nodeType": "VariableDeclaration",
                      "scope": 4824,
                      "src": "1434:16:6",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 4793,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1434:7:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 4799,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 4797,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1489:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 4795,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4667,
                        "src": "1453:15:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 4796,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getPositionFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10569,
                      "src": "1453:35:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_uint256_$",
                        "typeString": "function (string memory) view external returns (uint256)"
                      }
                    },
                    "id": 4798,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1453:43:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1434:62:6"
                },
                {
                  "assignments": [
                    4801
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 4801,
                      "name": "version",
                      "nodeType": "VariableDeclaration",
                      "scope": 4824,
                      "src": "1503:15:6",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 4800,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1503:7:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 4807,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 4804,
                        "name": "agentCreator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4787,
                        "src": "1553:12:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 4805,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4794,
                        "src": "1567:8:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 4802,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4667,
                        "src": "1521:15:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 4803,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentVersion",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10587,
                      "src": "1521:31:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_uint256_$",
                        "typeString": "function (address,uint256) view external returns (uint256)"
                      }
                    },
                    "id": 4806,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1521:55:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1503:73:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4815,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 4808,
                          "name": "agentVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4663,
                          "src": "1583:13:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 4812,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 4809,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12868,
                            "src": "1597:3:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 4810,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1597:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1583:25:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 4813,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 4811,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1609:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1583:33:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 4814,
                      "name": "version",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4801,
                      "src": "1619:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1583:43:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 4816,
                  "nodeType": "ExpressionStatement",
                  "src": "1583:43:6"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 4818,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12868,
                          "src": "1658:3:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 4819,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1658:10:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 4820,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1670:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 4817,
                      "name": "ChangedAgentVersion",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10648,
                      "src": "1638:19:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 4821,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1638:39:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4822,
                  "nodeType": "EmitStatement",
                  "src": "1633:44:6"
                }
              ]
            },
            "documentation": null,
            "id": 4824,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4784,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4783,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4684,
                  "src": "1341:14:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1341:14:6"
              }
            ],
            "name": "updateAgentVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4782,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4781,
                  "name": "_agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 4824,
                  "src": "1319:13:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 4780,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1319:6:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1318:15:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4785,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1356:0:6"
            },
            "scope": 4864,
            "src": "1291:392:6",
            "stateMutability": "nonpayable",
            "superFunction": 10669,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4846,
              "nodeType": "Block",
              "src": "1762:94:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4838,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 4831,
                          "name": "agentVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4663,
                          "src": "1769:13:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 4835,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 4832,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12868,
                            "src": "1783:3:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 4833,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1783:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1769:25:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 4836,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 4834,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4826,
                        "src": "1795:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1769:33:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 4837,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1805:1:6",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "src": "1769:37:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 4839,
                  "nodeType": "ExpressionStatement",
                  "src": "1769:37:6"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 4841,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12868,
                          "src": "1831:3:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 4842,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1831:10:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 4843,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4826,
                        "src": "1843:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 4840,
                      "name": "ClearedAgent",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10654,
                      "src": "1818:12:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 4844,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1818:32:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4845,
                  "nodeType": "EmitStatement",
                  "src": "1813:37:6"
                }
              ]
            },
            "documentation": null,
            "id": 4847,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4829,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4828,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4684,
                  "src": "1747:14:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1747:14:6"
              }
            ],
            "name": "clearAgent",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4827,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4826,
                  "name": "_agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 4847,
                  "src": "1725:13:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 4825,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1725:6:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1724:15:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4830,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1762:0:6"
            },
            "scope": 4864,
            "src": "1705:151:6",
            "stateMutability": "nonpayable",
            "superFunction": 10674,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4862,
              "nodeType": "Block",
              "src": "1978:46:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 4856,
                        "name": "agentVersions",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4663,
                        "src": "1992:13:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                          "typeString": "mapping(address => mapping(string memory => uint256))"
                        }
                      },
                      "id": 4858,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 4857,
                        "name": "host",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4849,
                        "src": "2006:4:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "1992:19:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                        "typeString": "mapping(string memory => uint256)"
                      }
                    },
                    "id": 4860,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 4859,
                      "name": "agent",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4851,
                      "src": "2012:5:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_memory_ptr",
                        "typeString": "string memory"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "1992:26:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 4855,
                  "id": 4861,
                  "nodeType": "Return",
                  "src": "1985:33:6"
                }
              ]
            },
            "documentation": null,
            "id": 4863,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getHostAgentVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4852,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4849,
                  "name": "host",
                  "nodeType": "VariableDeclaration",
                  "scope": 4863,
                  "src": "1920:12:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4848,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1920:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 4851,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 4863,
                  "src": "1934:12:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 4850,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1934:6:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1919:28:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4855,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4854,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 4863,
                  "src": "1969:7:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 4853,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1969:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1968:9:6"
            },
            "scope": 4864,
            "src": "1891:133:6",
            "stateMutability": "view",
            "superFunction": 10683,
            "visibility": "public"
          }
        ],
        "scope": 4865,
        "src": "204:1823:6"
      }
    ],
    "src": "0:2028:6"
  },
  "legacyAST": {
    "absolutePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/hosts/AgentVersioning.sol",
    "exportedSymbols": {
      "AgentVersioning": [
        4864
      ]
    },
    "id": 4865,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 4652,
        "literals": [
          "solidity",
          "0.4",
          ".24"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:23:6"
      },
      {
        "absolutePath": "contracts/interfaces/IAgentManagement.sol",
        "file": "contracts/interfaces/IAgentManagement.sol",
        "id": 4653,
        "nodeType": "ImportDirective",
        "scope": 4865,
        "sourceUnit": 10641,
        "src": "25:51:6",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IStaking.sol",
        "file": "contracts/interfaces/IStaking.sol",
        "id": 4654,
        "nodeType": "ImportDirective",
        "scope": 4865,
        "sourceUnit": 12004,
        "src": "77:43:6",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IAgentVersioning.sol",
        "file": "contracts/interfaces/IAgentVersioning.sol",
        "id": 4655,
        "nodeType": "ImportDirective",
        "scope": 4865,
        "sourceUnit": 10685,
        "src": "121:51:6",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 4656,
              "name": "IAgentVersioning",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10684,
              "src": "232:16:6",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentVersioning_$10684",
                "typeString": "contract IAgentVersioning"
              }
            },
            "id": 4657,
            "nodeType": "InheritanceSpecifier",
            "src": "232:16:6"
          }
        ],
        "contractDependencies": [
          10684
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 4864,
        "linearizedBaseContracts": [
          4864,
          10684
        ],
        "name": "AgentVersioning",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "constant": false,
            "id": 4663,
            "name": "agentVersions",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "254:60:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
              "typeString": "mapping(address => mapping(string => uint256))"
            },
            "typeName": {
              "id": 4662,
              "keyType": {
                "id": 4658,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "262:7:6",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "254:46:6",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                "typeString": "mapping(address => mapping(string => uint256))"
              },
              "valueType": {
                "id": 4661,
                "keyType": {
                  "id": 4659,
                  "name": "string",
                  "nodeType": "ElementaryTypeName",
                  "src": "281:6:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_storage_ptr",
                    "typeString": "string"
                  }
                },
                "nodeType": "Mapping",
                "src": "273:26:6",
                "typeDescriptions": {
                  "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                  "typeString": "mapping(string => uint256)"
                },
                "valueType": {
                  "id": 4660,
                  "name": "uint256",
                  "nodeType": "ElementaryTypeName",
                  "src": "291:7:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 4665,
            "name": "owner",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "319:13:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_address",
              "typeString": "address"
            },
            "typeName": {
              "id": 4664,
              "name": "address",
              "nodeType": "ElementaryTypeName",
              "src": "319:7:6",
              "typeDescriptions": {
                "typeIdentifier": "t_address",
                "typeString": "address"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 4667,
            "name": "agentManagement",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "337:32:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IAgentManagement_$10640",
              "typeString": "contract IAgentManagement"
            },
            "typeName": {
              "contractScope": null,
              "id": 4666,
              "name": "IAgentManagement",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10640,
              "src": "337:16:6",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                "typeString": "contract IAgentManagement"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 4669,
            "name": "staking",
            "nodeType": "VariableDeclaration",
            "scope": 4864,
            "src": "373:16:6",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IStaking_$12003",
              "typeString": "contract IStaking"
            },
            "typeName": {
              "contractScope": null,
              "id": 4668,
              "name": "IStaking",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 12003,
              "src": "373:8:6",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IStaking_$12003",
                "typeString": "contract IStaking"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 4683,
              "nodeType": "Block",
              "src": "420:114:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 4678,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "id": 4674,
                                "name": "msg",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 12868,
                                "src": "464:3:6",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_magic_message",
                                  "typeString": "msg"
                                }
                              },
                              "id": 4675,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "sender",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": null,
                              "src": "464:10:6",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "expression": {
                              "argumentTypes": null,
                              "id": 4672,
                              "name": "staking",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 4669,
                              "src": "435:7:6",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_IStaking_$12003",
                                "typeString": "contract IStaking"
                              }
                            },
                            "id": 4673,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "getIsCurrentlyStaked",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 11978,
                            "src": "435:28:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_external_view$_t_address_$returns$_t_bool_$",
                              "typeString": "function (address) view external returns (bool)"
                            }
                          },
                          "id": 4676,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "435:40:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 4677,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "479:4:6",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "435:48:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652073656e64657220646f6573206e6f7420686176652061207374616b65",
                        "id": 4679,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "485:34:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_1c989b5bdcc741547254981a1d4be4e836c3a58c33d76e8e4107321beb7cee3e",
                          "typeString": "literal_string \"The sender does not have a stake\""
                        },
                        "value": "The sender does not have a stake"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_1c989b5bdcc741547254981a1d4be4e836c3a58c33d76e8e4107321beb7cee3e",
                          "typeString": "literal_string \"The sender does not have a stake\""
                        }
                      ],
                      "id": 4671,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "427:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4680,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "427:93:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4681,
                  "nodeType": "ExpressionStatement",
                  "src": "427:93:6"
                },
                {
                  "id": 4682,
                  "nodeType": "PlaceholderStatement",
                  "src": "527:1:6"
                }
              ]
            },
            "documentation": null,
            "id": 4684,
            "name": "onlyStakedHost",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 4670,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "417:2:6"
            },
            "src": "394:140:6",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 4695,
              "nodeType": "Block",
              "src": "559:80:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 4690,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 4687,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12868,
                            "src": "574:3:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 4688,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "574:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "id": 4689,
                          "name": "owner",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4665,
                          "src": "588:5:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "574:19:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652073656e646572206973206e6f7420746865206f776e6572",
                        "id": 4691,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "595:29:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_8d0db2b1ac9d2dc378f870c93c1be80e6480134cdf13829b212cb3e504e73504",
                          "typeString": "literal_string \"The sender is not the owner\""
                        },
                        "value": "The sender is not the owner"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_8d0db2b1ac9d2dc378f870c93c1be80e6480134cdf13829b212cb3e504e73504",
                          "typeString": "literal_string \"The sender is not the owner\""
                        }
                      ],
                      "id": 4686,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "566:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4692,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "566:59:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4693,
                  "nodeType": "ExpressionStatement",
                  "src": "566:59:6"
                },
                {
                  "id": 4694,
                  "nodeType": "PlaceholderStatement",
                  "src": "632:1:6"
                }
              ]
            },
            "documentation": null,
            "id": 4696,
            "name": "onlyOwner",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 4685,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "556:2:6"
            },
            "src": "538:101:6",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 4720,
              "nodeType": "Block",
              "src": "693:119:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4706,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4703,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4665,
                      "src": "700:5:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "id": 4704,
                        "name": "msg",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12868,
                        "src": "708:3:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_magic_message",
                          "typeString": "msg"
                        }
                      },
                      "id": 4705,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "sender",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "708:10:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "700:18:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 4707,
                  "nodeType": "ExpressionStatement",
                  "src": "700:18:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4712,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4708,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4667,
                      "src": "725:15:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4710,
                          "name": "management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4698,
                          "src": "760:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4709,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10640,
                        "src": "743:16:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10640_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 4711,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "743:28:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "725:46:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 4713,
                  "nodeType": "ExpressionStatement",
                  "src": "725:46:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4718,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4714,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4669,
                      "src": "778:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4716,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4700,
                          "src": "797:8:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4715,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12003,
                        "src": "788:8:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$12003_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 4717,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "788:18:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "778:28:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$12003",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 4719,
                  "nodeType": "ExpressionStatement",
                  "src": "778:28:6"
                }
              ]
            },
            "documentation": null,
            "id": 4721,
            "implemented": true,
            "isConstructor": true,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4701,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4698,
                  "name": "management",
                  "nodeType": "VariableDeclaration",
                  "scope": 4721,
                  "src": "655:18:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4697,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "655:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 4700,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 4721,
                  "src": "675:16:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4699,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "675:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "654:38:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4702,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "693:0:6"
            },
            "scope": 4864,
            "src": "643:169:6",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4732,
              "nodeType": "Block",
              "src": "866:30:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4730,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4728,
                      "name": "owner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4665,
                      "src": "873:5:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 4729,
                      "name": "_newOwner",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4723,
                      "src": "881:9:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "873:17:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 4731,
                  "nodeType": "ExpressionStatement",
                  "src": "873:17:6"
                }
              ]
            },
            "documentation": null,
            "id": 4733,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4726,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4725,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4696,
                  "src": "856:9:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "856:9:6"
              }
            ],
            "name": "changeOwner",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4724,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4723,
                  "name": "_newOwner",
                  "nodeType": "VariableDeclaration",
                  "scope": 4733,
                  "src": "837:17:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4722,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "837:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "836:19:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4727,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "866:0:6"
            },
            "scope": 4864,
            "src": "816:80:6",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4755,
              "nodeType": "Block",
              "src": "961:133:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 4745,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 4741,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4735,
                          "src": "976:11:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 4743,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "999:1:6",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 4742,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "991:7:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 4744,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "991:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "976:25:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "546865206d616e6167656d656e7420706172616d206973206e756c6c",
                        "id": 4746,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1003:30:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_b76c630f75a5f51b4f5c44c4e1b24521c7d74b12ec894b4e7082c4c230dd3d46",
                          "typeString": "literal_string \"The management param is null\""
                        },
                        "value": "The management param is null"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_b76c630f75a5f51b4f5c44c4e1b24521c7d74b12ec894b4e7082c4c230dd3d46",
                          "typeString": "literal_string \"The management param is null\""
                        }
                      ],
                      "id": 4740,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "968:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4747,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "968:66:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4748,
                  "nodeType": "ExpressionStatement",
                  "src": "968:66:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4753,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4749,
                      "name": "agentManagement",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4667,
                      "src": "1041:15:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4751,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4735,
                          "src": "1076:11:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4750,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10640,
                        "src": "1059:16:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10640_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 4752,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1059:29:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "1041:47:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 4754,
                  "nodeType": "ExpressionStatement",
                  "src": "1041:47:6"
                }
              ]
            },
            "documentation": null,
            "id": 4756,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4738,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4737,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4696,
                  "src": "951:9:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "951:9:6"
              }
            ],
            "name": "setManagement",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4736,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4735,
                  "name": "_management",
                  "nodeType": "VariableDeclaration",
                  "scope": 4756,
                  "src": "923:19:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4734,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "923:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "922:21:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4739,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "961:0:6"
            },
            "scope": 4864,
            "src": "900:194:6",
            "stateMutability": "nonpayable",
            "superFunction": 10659,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4778,
              "nodeType": "Block",
              "src": "1153:116:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 4768,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 4764,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4758,
                          "src": "1168:8:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "!=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "hexValue": "30",
                              "id": 4766,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "kind": "number",
                              "lValueRequested": false,
                              "nodeType": "Literal",
                              "src": "1188:1:6",
                              "subdenomination": null,
                              "typeDescriptions": {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              },
                              "value": "0"
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_rational_0_by_1",
                                "typeString": "int_const 0"
                              }
                            ],
                            "id": 4765,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "1180:7:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 4767,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "1180:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "1168:22:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "546865207374616b696e67206164647265737320706172616d206973206e756c6c",
                        "id": 4769,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "1192:35:6",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_bbb348a445a9181fd72bec99bddf455bf37630cdf960961a020d9caee222fc64",
                          "typeString": "literal_string \"The staking address param is null\""
                        },
                        "value": "The staking address param is null"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_bbb348a445a9181fd72bec99bddf455bf37630cdf960961a020d9caee222fc64",
                          "typeString": "literal_string \"The staking address param is null\""
                        }
                      ],
                      "id": 4763,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        12871,
                        12872
                      ],
                      "referencedDeclaration": 12872,
                      "src": "1160:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 4770,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1160:68:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4771,
                  "nodeType": "ExpressionStatement",
                  "src": "1160:68:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4776,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 4772,
                      "name": "staking",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4669,
                      "src": "1235:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 4774,
                          "name": "_staking",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4758,
                          "src": "1254:8:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 4773,
                        "name": "IStaking",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 12003,
                        "src": "1245:8:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IStaking_$12003_$",
                          "typeString": "type(contract IStaking)"
                        }
                      },
                      "id": 4775,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "1245:18:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IStaking_$12003",
                        "typeString": "contract IStaking"
                      }
                    },
                    "src": "1235:28:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IStaking_$12003",
                      "typeString": "contract IStaking"
                    }
                  },
                  "id": 4777,
                  "nodeType": "ExpressionStatement",
                  "src": "1235:28:6"
                }
              ]
            },
            "documentation": null,
            "id": 4779,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4761,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4760,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4696,
                  "src": "1143:9:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1143:9:6"
              }
            ],
            "name": "setStaking",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4759,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4758,
                  "name": "_staking",
                  "nodeType": "VariableDeclaration",
                  "scope": 4779,
                  "src": "1118:16:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4757,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1118:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1117:18:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4762,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1153:0:6"
            },
            "scope": 4864,
            "src": "1098:171:6",
            "stateMutability": "nonpayable",
            "superFunction": 10664,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4823,
              "nodeType": "Block",
              "src": "1356:327:6",
              "statements": [
                {
                  "assignments": [
                    4787
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 4787,
                      "name": "agentCreator",
                      "nodeType": "VariableDeclaration",
                      "scope": 4824,
                      "src": "1363:20:6",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 4786,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "1363:7:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 4792,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 4790,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1421:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 4788,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4667,
                        "src": "1386:15:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 4789,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getCreatorFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10562,
                      "src": "1386:34:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_address_$",
                        "typeString": "function (string memory) view external returns (address)"
                      }
                    },
                    "id": 4791,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1386:42:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1363:65:6"
                },
                {
                  "assignments": [
                    4794
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 4794,
                      "name": "position",
                      "nodeType": "VariableDeclaration",
                      "scope": 4824,
                      "src": "1434:16:6",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 4793,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1434:7:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 4799,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 4797,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1489:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 4795,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4667,
                        "src": "1453:15:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 4796,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getPositionFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10569,
                      "src": "1453:35:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_uint256_$",
                        "typeString": "function (string memory) view external returns (uint256)"
                      }
                    },
                    "id": 4798,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1453:43:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1434:62:6"
                },
                {
                  "assignments": [
                    4801
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 4801,
                      "name": "version",
                      "nodeType": "VariableDeclaration",
                      "scope": 4824,
                      "src": "1503:15:6",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 4800,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1503:7:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 4807,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 4804,
                        "name": "agentCreator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4787,
                        "src": "1553:12:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 4805,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4794,
                        "src": "1567:8:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 4802,
                        "name": "agentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4667,
                        "src": "1521:15:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10640",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 4803,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentVersion",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10587,
                      "src": "1521:31:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_uint256_$",
                        "typeString": "function (address,uint256) view external returns (uint256)"
                      }
                    },
                    "id": 4806,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1521:55:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1503:73:6"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4815,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 4808,
                          "name": "agentVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4663,
                          "src": "1583:13:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 4812,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 4809,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12868,
                            "src": "1597:3:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 4810,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1597:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1583:25:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 4813,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 4811,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1609:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1583:33:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 4814,
                      "name": "version",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4801,
                      "src": "1619:7:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1583:43:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 4816,
                  "nodeType": "ExpressionStatement",
                  "src": "1583:43:6"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 4818,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12868,
                          "src": "1658:3:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 4819,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1658:10:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 4820,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4781,
                        "src": "1670:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 4817,
                      "name": "ChangedAgentVersion",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10648,
                      "src": "1638:19:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 4821,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1638:39:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4822,
                  "nodeType": "EmitStatement",
                  "src": "1633:44:6"
                }
              ]
            },
            "documentation": null,
            "id": 4824,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4784,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4783,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4684,
                  "src": "1341:14:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1341:14:6"
              }
            ],
            "name": "updateAgentVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4782,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4781,
                  "name": "_agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 4824,
                  "src": "1319:13:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 4780,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1319:6:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1318:15:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4785,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1356:0:6"
            },
            "scope": 4864,
            "src": "1291:392:6",
            "stateMutability": "nonpayable",
            "superFunction": 10669,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4846,
              "nodeType": "Block",
              "src": "1762:94:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 4838,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 4831,
                          "name": "agentVersions",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 4663,
                          "src": "1769:13:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                            "typeString": "mapping(address => mapping(string memory => uint256))"
                          }
                        },
                        "id": 4835,
                        "indexExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 4832,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 12868,
                            "src": "1783:3:6",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 4833,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "1783:10:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1769:25:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                          "typeString": "mapping(string memory => uint256)"
                        }
                      },
                      "id": 4836,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 4834,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4826,
                        "src": "1795:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "1769:33:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 4837,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1805:1:6",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "src": "1769:37:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 4839,
                  "nodeType": "ExpressionStatement",
                  "src": "1769:37:6"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 4841,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 12868,
                          "src": "1831:3:6",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 4842,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "1831:10:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 4843,
                        "name": "_agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4826,
                        "src": "1843:6:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "id": 4840,
                      "name": "ClearedAgent",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10654,
                      "src": "1818:12:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (address,string memory)"
                      }
                    },
                    "id": 4844,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1818:32:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 4845,
                  "nodeType": "EmitStatement",
                  "src": "1813:37:6"
                }
              ]
            },
            "documentation": null,
            "id": 4847,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [
              {
                "arguments": null,
                "id": 4829,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 4828,
                  "name": "onlyStakedHost",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 4684,
                  "src": "1747:14:6",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "1747:14:6"
              }
            ],
            "name": "clearAgent",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4827,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4826,
                  "name": "_agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 4847,
                  "src": "1725:13:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 4825,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1725:6:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1724:15:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4830,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "1762:0:6"
            },
            "scope": 4864,
            "src": "1705:151:6",
            "stateMutability": "nonpayable",
            "superFunction": 10674,
            "visibility": "public"
          },
          {
            "body": {
              "id": 4862,
              "nodeType": "Block",
              "src": "1978:46:6",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 4856,
                        "name": "agentVersions",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4663,
                        "src": "1992:13:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_mapping$_t_string_memory_$_t_uint256_$_$",
                          "typeString": "mapping(address => mapping(string memory => uint256))"
                        }
                      },
                      "id": 4858,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 4857,
                        "name": "host",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 4849,
                        "src": "2006:4:6",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "1992:19:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_string_memory_$_t_uint256_$",
                        "typeString": "mapping(string memory => uint256)"
                      }
                    },
                    "id": 4860,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 4859,
                      "name": "agent",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 4851,
                      "src": "2012:5:6",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_memory_ptr",
                        "typeString": "string memory"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "1992:26:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 4855,
                  "id": 4861,
                  "nodeType": "Return",
                  "src": "1985:33:6"
                }
              ]
            },
            "documentation": null,
            "id": 4863,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getHostAgentVersion",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 4852,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4849,
                  "name": "host",
                  "nodeType": "VariableDeclaration",
                  "scope": 4863,
                  "src": "1920:12:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 4848,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "1920:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 4851,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 4863,
                  "src": "1934:12:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 4850,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "1934:6:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1919:28:6"
            },
            "payable": false,
            "returnParameters": {
              "id": 4855,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 4854,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 4863,
                  "src": "1969:7:6",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 4853,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1969:7:6",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1968:9:6"
            },
            "scope": 4864,
            "src": "1891:133:6",
            "stateMutability": "view",
            "superFunction": 10683,
            "visibility": "public"
          }
        ],
        "scope": 4865,
        "src": "204:1823:6"
      }
    ],
    "src": "0:2028:6"
  },
  "compiler": {
    "name": "solc",
    "version": "0.4.24+commit.e67f0147.Emscripten.clang"
  },
  "networks": {
    "1546036678594": {
      "events": {},
      "links": {},
      "address": "0x4eb87c820706c164330170d463ca6835b8966657",
      "transactionHash": "0x661d0f646441e8b080669444c06004369bdfc4f8b09be8c7feddaed7ae4fb8ea"
    }
  },
  "schemaVersion": "2.0.1",
  "updatedAt": "2018-12-28T22:39:10.063Z"
}

module.exports = {

  agentVersioning

}
