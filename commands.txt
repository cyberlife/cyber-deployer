DEPLOYER:

node deployer.js --cli='/home/stefan/Work/cyberlife-js/agents/smartAgents/deployercli/bin/run ' --privateKey='' --pubKey='7eff122b94897ea5b0e2a9abf47b86337fafebdc' --address='/ip4/0.0.0.0/tcp/0' --id=1


RESERVE SIMPLE AGENT NAME:

agent newAgent -n=Simple_Agent -s=https://gitlab.com/universenetwork/simple-agent.git -a=0x62e655f081ec03e0115fb5b59da9f3c3f22de1c90ee21554b28f71cde06c89fc


RESERVE LENDING AGENT NAME:

agent newAgent -n=Lending -s=https://gitlab.com/cyberlife/lending-smart-agent.git -a=0xbd655f081ec03e0115fb5b59da9f3c3f22de1c90ee21554b28f71cde06c89fc



DEPLOY SIMPLE AGENT:

agent deploy -n=Simple_Agent -t=6000 -r=1

DEPLOY LENDING

agent deploy -n=Lending -t=6000 -r=1


WITHDRAW SIMPLE_AGENT:

agent withdraw -n=Simple_Agent -o=1

WITHDRAW LENDING:

agent withdraw -n=Lending -o=1
