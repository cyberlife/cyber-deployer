var Web3 = require('web3');
var fs = require('fs'), readline = require('readline');

var utils = require('ethereumjs-util');
var BigNumber = require('bignumber.js');

var contractModel = require("./contractModel")

//SETUP

var web3

require('dotenv').config({ path: '../.env' })

var DEFAULT_GAS = process.env.DEFAULT_GAS
var DEFAULT_ADDRESS = process.env.DEFAULT_ADDRESS
var defaultAddress = process.env.MAIN_CHAIN_ACCOUNT

if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
  } else {
    // Set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider(process.env.LOCAL_CHAIN));
}

//GENERAL UTILS

function readFile(filePath) {

  var lines = []

  fs.readFileSync(filePath).toString().split('\n').forEach(function (line) { lines.push(line) })

  return lines

}

async function getCurrentBlockNr(callback) {

  web3.eth.getBlockNumber(function (err, blockNr) {

    if (err != undefined && err != null) return callback(err, undefined)

    return callback(undefined, blockNr)

  })

}

//GET CONTRACTS INSTANCES

async function getContract(pubKey, name) {

  var contractDict = contractModel.contractDict;

  if (contractDict[name] == undefined)
    return undefined;

  var contractInstance =
    new web3.eth.Contract(
      contractDict[name][0].abi,
      contractDict[name][1],
      {from: pubKey, gas: DEFAULT_GAS})

  return contractInstance;

}

//EPHEMERAL PERMISSIONS

async function proposePermission(pubKey, agent, paramsHash,
  executionProof, forContract, forFunc, hostingProof) {

  var permissions = await getContract(pubKey, "ephemeralPermissions");

  await permissions.methods.proposePermission(agent, paramsHash, executionProof, forContract, forFunc, hostingProof)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {})
    .on('error', console.error);

}

async function usePermission(pubKey, agent, position) {

  var permissions = await getContract(pubKey, "ephemeralPermissions");

  await permissions.methods.usePermission(agent, position)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {})
    .on('error', console.error);

}

async function canUsePermission(agent, position, contract, func, caller, paramsHash) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var canUse

  canUse = await permissions.methods.canUsePermission(agent, position, contract, func, caller, paramsHash)
    .call({from: defaultAddress})

  return canUse

}

async function getLatestNonce(agent) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var nonce

  nonce = await permissions.methods.getLatestNonce(agent)
    .call({from: defaultAddress})

  return nonce

}

async function getContractWhereUsed(agent, position) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var contract

  contract = await permissions.methods.getContractWhereUsed(agent, position)
    .call({from: defaultAddress})

  return contract

}

async function getFunctionWhereUsed(agent, position) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var func

  func = await permissions.methods.getFunctionWhereUsed(agent, position)
    .call({from: defaultAddress})

  return func

}

async function getHashOfParams(agent, position) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var hash

  hash = await permissions.methods.getHashOfParams(agent, position)
    .call({from: defaultAddress})

  return hash

}

async function getTrustedExecutionProof(agent, position) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var proof

  proof = await permissions.methods.getTrustedExecutionProof(agent, position)
    .call({from: defaultAddress})

  return proof

}

async function getCreationTime(agent, position) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var creation

  creation = await permissions.methods.getCreationTime(agent, position)
    .call({from: defaultAddress})

  return creation

}

async function getUsed(agent, position) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var used

  used = await permissions.methods.getUsed(agent, position)
    .call({from: defaultAddress})

  return used

}

async function getHost(agent, position) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var host

  host = await permissions.methods.getHost(agent, position)
    .call({from: defaultAddress})

  return host

}

async function getAvailability() {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var availability

  availability = await permissions.methods.getAvailability()
    .call({from: defaultAddress})

  return availability

}

async function getContractAllowance(con) {

  var permissions = await getContract(DEFAULT_ADDRESS, "ephemeralPermissions");

  var allowance

  allowance = await permissions.methods.getContractAllowance(con)
    .call({from: defaultAddress})

  return allowance

}

//ASSIGNING JOBS

async function changingAgentTree(pubKey, _agentName) {

  if (pubKey == undefined || _agentName == undefined)
    {console.log("Please call with correct params"); return}

  var jobs = await getContract(pubKey, "assigningJobs");

  await jobs.methods.changingAgentTree(_agentName)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {})
    .on('error', console.error);

}

async function finishedChangingTree(pubKey, _agentName) {

  if (pubKey == undefined || _agentName == undefined)
    {console.log("Please call with correct params"); return}

  var jobs = await getContract(pubKey, "assigningJobs");

  await jobs.methods.finishedChangingTree(_agentName)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {})
    .on('error', console.error);

}

async function getChangingTree(agent) {

  var jobs = await getContract(DEFAULT_ADDRESS, "assigningJobs");

  var migrating

  migrating = await jobs.methods.getChangingTree(agent)
    .call({from: defaultAddress});

  return migrating

}

async function getHostChangingTree(agent) {

  var jobs = await getContract(DEFAULT_ADDRESS, "assigningJobs");

  var host

  host = await jobs.methods.getHostChangingTree(agent)
    .call({from: defaultAddress});

  return host

}

async function getIsChangingTree(host) {

  var jobs = await getContract(DEFAULT_ADDRESS, "assigningJobs");

  var resp

  resp = await jobs.methods.getIsChangingTree(host).call({from: defaultAddress});

  return resp

}

//AGENT VERSIONING

async function updateAgentVersion(pubKey, _agentName) {

  if (pubKey == undefined || _agentName == undefined)
    {console.log("Please call with correct params"); return}

  var versioning = await getContract(pubKey, "agentVersioning");

  await versioning.methods.updateAgentVersion(_agentName)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {})
    .on('error', console.error);

}

async function clearAgent(pubKey, _agentName) {

  if (pubKey == undefined || _agentName == undefined)
    {console.log("Please call with correct params"); return}

  var versioning = await getContract(pubKey, "agentVersioning");

  await versioning.methods.clearAgent(_agentName)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log(_agentName + " was deleted on-chain")

    })
    .on('error', console.error);

}

async function getHostAgentVersion(host, agent) {

  var versioning = await getContract(DEFAULT_ADDRESS, "agentVersioning");

  var ver

  ver = await versioning.methods.getHostAgentVersion(host, agent)
    .call({from: defaultAddress});

  return ver

}

//AGENTS TREASURY

async function setTreasuryShardModifications(pubKey, shardModif) {

  if (pubKey == undefined || shardModif == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.setShardModifications(shardModif)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The shard modifications address in treasury was changed")

    })
    .on('error', console.error);

}

async function setHostManagement(pubKey, hostManagement) {

  if (pubKey == undefined || shardModif == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.setHostManagement(hostManagement)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The host management address in treasury was changed")

    })
    .on('error', console.error);

}

async function setTreasuryDeploymentRequests(pubKey, deploymentR) {

  if (pubKey == undefined || deploymentR == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.changeDeploymentRequestAddr(deploymentR)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The deployment requests address in treasury was changed")

    })
    .on('error', console.error);

}

async function changePaymentWindow(pubKey, _window) {

  if (pubKey == undefined || _window == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.changePaymentWindow(_window)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The payment window in treasury was changed")

    })
    .on('error', console.error);

}

async function changeTreasuryAgentManagement(pubKey, agentManagement) {

  if (pubKey == undefined || agentManagement == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.changeAgentManagement(agentManagement)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The agent management address in treasury was changed")

    })
    .on('error', console.error);

}

async function registerAgentTr(pubKey, name, root, ipfs, hostsNr, money) {

  if (pubKey == undefined || name == undefined || root == undefined || ipfs == undefined
      || hostsNr == undefined || money == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.registerAgent(name, root, ipfs, hostsNr)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS, value: money})
    .on('receipt', (receipt) => {

      console.log(name + " is registered")

    })
    .on('error', console.error);

}

async function applyModificationTr(pubKey, name, intData, root, ipfs, hosts, deadlines, currentPrices) {

  if (pubKey == undefined || name == undefined || intData == undefined || root == undefined || ipfs == undefined
      || hosts == undefined || deadlines == undefined || currentPrices == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.applyModification(name, intData, root, ipfs, hosts, deadlines, currentPrices)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("A modification for " + name + " was applied")

    })
    .on('error', console.error);

}

async function fundAgent(pubKey, name, _value) {

  if (pubKey == undefined || name == undefined || _value == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.fund(name)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS, value: _value})
    .on('receipt', (receipt) => {

      console.log("The agent called " + name + " was funded with " + _value + " wei")

    })
    .on('error', console.error);

}

async function withdrawFunds(pubKey, name) {

  if (pubKey == undefined || name == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.withdrawFunds(name)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("All funds were withdrawn from " + name + " and sent to " + pubKey)

    })
    .on('error', console.error);

}

async function getPaid(pubKey, name, proof, timeStartedHostingAgent, secondsToHost, pricePerSecond) {

  if (pubKey == undefined || name == undefined || timeStartedHostingAgent == undefined || secondsToHost == undefined
      || pricePerSecond == undefined || proof == undefined)
    {console.log("Please call with correct params"); return}

  var tr = await getContract(pubKey, "agentTreasury");

  await tr.methods.getPaid(name, proof, timeStartedHostingAgent, secondsToHost, pricePerSecond)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("Got paid for hosting " + name)

    })
    .on('error', console.error);

}

async function getSecondsToBill(host, agent, secondsToHost) {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var seconds

  seconds = await tr.methods.getSecondsToBill(host, agent, secondsToHost)
    .call({from: defaultAddress});

  return seconds

}

async function getAgentManagementTreasury() {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var addr

  addr = await tr.methods.getAgentManagementAddr()
    .call({from: defaultAddress});

  return addr

}

async function getPaymentWindow() {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var _window

  _window = await tr.methods.getPaymentWindow().call({from: defaultAddress});

  return _window

}

async function getSecondsBilled(host, agentName) {

  if (host == undefined || agentName == undefined) return undefined

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var seconds

  seconds = await tr.methods.getSecondsBilled(host, agentName)
    .call({from: defaultAddress});

  return seconds

}

async function agentAlreadyRegistered(name) {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var registered

  registered = await tr.methods.agentAlreadyRegistered(name).call({from: defaultAddress});

  return registered

}

async function getAgentIntData(name) {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var intData

  intData = await tr.methods.getAgentIntData(name).call({from: defaultAddress});

  return intData

}

async function getAgentRoot(name) {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var root

  root = await tr.methods.getAgentRoot(name).call({from: defaultAddress});

  return root

}

async function agentIsRegistered(name) {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var registered

  registered = await tr.methods.agentIsRegistered(name).call({from: defaultAddress});

  return registered

}

async function getContribution(agent, name) {

  var tr = await getContract(DEFAULT_ADDRESS, "agentTreasury");

  var contribution

  contribution = await tr.methods.getContribution(agent, name)
    .call({from: defaultAddress});

  return contribution

}

//SHARD MODIFICATIONS

async function setShardTreasury(pubKey, treasury) {

  if (pubKey == undefined || treasury == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.setTreasury(treasury)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The treasury address in shard modifications was changed into " + treasury)

    })
    .on('error', console.error);

}

async function setShardDeploymentReq(pubKey, deploymentR) {

  if (pubKey == undefined || deploymentR == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.changeDeploymentRequestAddr(deploymentR)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The deployment requests address inside shard modifications was changed into " + deploymentR)

    })
    .on('error', console.error);

}

async function changeVotingWindow(pubKey, _window) {

  if (pubKey == undefined || _window == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.changeVotingWindow(_window)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The shard modifications voting window was changed to " + _window)

    })
    .on('error', console.error);

}

async function changeShardAgentManagement(pubKey, management) {

  if (pubKey == undefined || management == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.changeAgentManagement(management)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The agent management address inside shard modifications was changed into " + management)

    })
    .on('error', console.error);

}

async function changeBatchSize(pubKey, batch) {

  if (pubKey == undefined || batch == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.changeBatchSize(batch)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The batch size inside shard modifications was changed into " + batch)

    })
    .on('error', console.error);

}

async function changeVoteThreshold(pubKey, threshold) {

  if (pubKey == undefined || threshold == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.changeVoteThreshold(threshold)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The vote threshold in shard modifications was changed into " + threshold)

    })
    .on('error', console.error);

}

async function proposeModification(pubKey, name, hosts, deadlines, currentPrices, root, ipfs, packagedInt) {

  if (pubKey == undefined || name == undefined || hosts == undefined || deadlines == undefined || currentPrices == undefined
      || root == undefined || ipfs == undefined || packagedInt == undefined)
    {console.log("\n" + "Please call proposeModification with correct params" + "\n"); return}

  if (hosts.length == 0 || deadlines.length == 0 || currentPrices.length == 0 || packagedInt.length != 3
      || hosts.length != deadlines.length || hosts.length != currentPrices.length)

    {console.log("\n" + "The arrays do not have the same length or one or more of them have no elements" + "\n"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.proposeModification(name, hosts, deadlines, currentPrices, root, ipfs, packagedInt)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("A new modification for " + name + " was proposed")

    })
    .on('error', console.error);

}

async function voteModification(pubKey, name, yes_no) {

  if (pubKey == undefined || name == undefined || yes_no == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.voteModification(name, yes_no)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("Voted for a modification on " + name)

    })
    .on('error', console.error);

}

async function applyModification(pubKey, name, money) {

  if (pubKey == undefined || name == undefined)
    {console.log("Please call with correct params"); return}

  var shardMod = await getContract(pubKey, "shardModifications");

  await shardMod.methods.applyModification(name)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS, value: money})
    .on('receipt', (receipt) => {

      console.log("A modification for " + name + " was applied")

    })
    .on('error', console.error);

}

async function getBatchSize() {

  var shardMod = await getContract(DEFAULT_ADDRESS, "shardModifications");

  var batch

  batch = await shardMod.methods.getBatchSize().call({from: defaultAddress});

  return batch

}

async function getVoteThreshold() {

  var shardMod = await getContract(DEFAULT_ADDRESS, "shardModifications");

  var threshold

  threshold = await shardMod.methods.getVoteThreshold()
    .call({from: defaultAddress});

  return threshold

}

async function getVotingWindow() {

  var shardMod = await getContract(DEFAULT_ADDRESS, "shardModifications");

  var _window

  _window = await shardMod.methods.getVotingWindow().call({from: defaultAddress});

  return _window

}

async function getModificationIntData(name) {

  var shardMod = await getContract(DEFAULT_ADDRESS, "shardModifications");

  var intData

  intData = await shardMod.methods.getModificationIntData(name)
    .call({from: defaultAddress});

  return intData

}

async function getModificationTree(agent) {

  var shardMod = await getContract(DEFAULT_ADDRESS, "shardModifications");

  var tree

  tree = await shardMod.methods.getModificationTree(agent)
    .call({from: defaultAddress});

  return tree

}

async function getModificationArrays(agent) {

  var shardMod = await getContract(DEFAULT_ADDRESS, "shardModifications");

  var arrays

  arrays = await shardMod.methods.getModificationArrays(agent)
    .call({from: defaultAddress});

  return arrays

}

async function agentVoted(agent, nonce) {

  var shardMod = await getContract(DEFAULT_ADDRESS, "shardModifications");

  var voted

  voted = await shardMod.methods.agentVoted(agent, nonce)
    .call({from: defaultAddress});

  return voted

}

//DEPLOYMENT REQUESTS

async function setShardModifications(pubKey, shardModif) {

  if (pubKey == undefined || shardModif == undefined)
    {console.log("Please call with correct params"); return}

  var deployReq = await getContract(pubKey, "deploymentRequests");

  await deployReq.methods.setShardModifications(shardModif)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The shard modifications in the deployment requests contract was changed into " + shardModif)

    })
    .on('error', console.error);

}

async function changeStakeAddress(pubKey, staking) {

  if (pubKey == undefined || staking == undefined)
    {console.log("Please call with correct params"); return}

  var deployReq = await getContract(pubKey, "deploymentRequests");

  await deployReq.methods.changeStakeAddress(staking)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The address of the staking contract in deployment requests was changed into " + staking)

    })
    .on('error', console.error);

}

async function changeAgentManagement(pubKey, agentMan) {

  if (pubKey == undefined || agentMan == undefined)
    {console.log("Please call with correct params"); return}

  var deployReq = await getContract(pubKey, "deploymentRequests");

  await deployReq.methods.changeAgentManagement(agentMan)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("The address of agent management in deployment requests was changed into " + agentMan)

    })
    .on('error', console.error);

}

async function deployAgent(pubKey, name, hostsRoot, hostsIpfs, billingRoot, billingIpfs, responsesNumber, moneyToDeposit) {

  if (pubKey == undefined || name == undefined || hostsRoot == undefined ||
      hostsIpfs == undefined || billingRoot == undefined || billingIpfs == undefined ||
      responsesNumber == undefined)

    {console.log("Please call with correct params"); return}

  var deployReq = await getContract(pubKey, "deploymentRequests");

  await deployReq.methods.deployAgent(name, hostsRoot, hostsIpfs, billingRoot,
    billingIpfs, responsesNumber, moneyToDeposit)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("A new deployment request for " + name + " was sent")

    })
    .on('error', console.error);

}

async function withdrawAgent(pubKey, name, root, ipfs, totalHostsNr) {

  if (pubKey == undefined || name == undefined || root == undefined ||
      ipfs == undefined || totalHostsNr == undefined)
    {console.log("Please call with correct params"); return}

  var deployReq = await getContract(pubKey, "deploymentRequests");

  await deployReq.methods.withdrawAgent(name, root, ipfs, totalHostsNr)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("A new withdrawal request for " + name + " was sent")

    })
    .on('error', console.error);

}

async function respondToRequest(pubKey, name, position, proof) {

  if (pubKey == undefined || name == undefined || position == undefined ||
      proof == undefined)
    {console.log("Please call with correct params"); return}

  var deployReq = await getContract(pubKey, "deploymentRequests");

  await deployReq.methods.respond(name, position, proof)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (receipt) => {

      console.log("Responded to a request for " + name)

    })
    .on('error', console.error);

}

async function getAgentManagementAddr() {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var addr

  addr = await deployReq.methods.getAgentManagementAddr()
    .call({from: defaultAddress});

  return addr

}

async function hasEnoughResponses(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var ok

  ok = await deployReq.methods.hasEnoughResponses(name, position)
    .call({from: defaultAddress});

  return ok

}

async function getDeploymentsLength(name) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var length

  length = await deployReq.methods.getDeploymentsLength(name)
    .call({from: defaultAddress});

  return length

}

async function getDeploymentIPFSProof(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var ipfs

  ipfs = await deployReq.methods.getDeploymentIPFSProof(name, position)
    .call({from: defaultAddress});

  return ipfs

}

async function getDeploymentHostsRoot(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var root

  root = await deployReq.methods.getDeploymentHostsRoot(name, position)
    .call({from: defaultAddress});

  return root

}

async function getDeploymentBillingRoot(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var root

  root = await deployReq.methods.getDeploymentBillingRoot(name, position)
    .call({from: defaultAddress});

  return root

}

async function getDeploymentBillingProof(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var proof

  proof = await deployReq.methods.getDeploymentBillingProof(name, position)
    .call({from: defaultAddress});

  return proof

}

async function getWithdrawalsLength(name) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var length

  length = await deployReq.methods.getWithdrawalsLength(name)
    .call({from: defaultAddress});

  return length

}

async function getWithdrawalRoot(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var root

  root = await deployReq.methods.getWithdrawalRoot(name, position)
    .call({from: defaultAddress});

  return root

}

async function getWithdrawalIPFSProof(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var ipfs

  ipfs = await deployReq.methods.getWithdrawalIPFSProof(name, position)
  .call({from: defaultAddress});

  return ipfs

}

async function getShardModifications() {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var addr

  addr = await deployReq.methods.getShardModifications()
  .call({from: defaultAddress});

  return addr

}

async function getDeploymentIntData(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var intData

  intData = await deployReq.methods.getDeploymentIntData(name, position)
    .call({from: defaultAddress});

  return intData

}

async function getDeploymentHostsTree(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var tree

  tree = await deployReq.methods.getDeploymentHostsTree(name, position)
    .call({from: defaultAddress});

  return tree

}

async function getWithdrawalData(name, position) {

  var deployReq = await getContract(DEFAULT_ADDRESS, "deploymentRequests");

  var data

  data = await deployReq.methods.getWithdrawalData(name, position)
    .call({from: defaultAddress});

  return data

}

//STAKING

async function stake(pubKey, privKey) {

  if (pubKey == undefined) {console.log("Please call with correct params"); return}

  var token = await getContract(pubKey, "workToken");

  var staking = await getContract(pubKey, "staking");

  var currentAllowance = new BigNumber()
  var isStaked = false
  var minimumStake = new BigNumber()

  token.methods.allowance(pubKey, addresses[1].toString())
  .call({from: defaultAddress})
  .then(async (currentAllowance) => {

    staking.methods.getCurrentlyStaked(pubKey)
    .call({from: defaultAddress})
    .then(async (isStaked) => {

      if (isStaked) {console.log("You already staked"); return}

      else {

        staking.methods.getStakeNeeded()
        .call({from: defaultAddress})
        .then(async (minimumStake) => {

          if (currentAllowance.isLessThan(minimumStake)) {

            token.methods.approve(addresses[1].toString(),
              minimumStake.minus(currentAllowance).toString())
              .send({from: pubKey.toString(), gas: DEFAULT_GAS})
              .on('receipt', (receipt) => {

                staking.methods.stake()
                .send({from: pubKey.toString(), gas: DEFAULT_GAS})
                .on('receipt', (stakeReceipt) => {

                  console.log("You just staked")

                })
                .on('error', console.error);

              })
              .on('error', console.error);

          }

        })

      }

    })

  })

}

async function signalStakeWithdrawal(pubKey) {

  if (pubKey == undefined)
    {console.log("Please call with correct params"); return}

  var staking = await getContract(pubKey, "staking");

  await staking.methods.signalWithdraw()
  .send({from: pubKey.toString(), gas: DEFAULT_GAS})
  .on('receipt', (stakeReceipt) => {

    console.log("Signalled that you want to withdraw your stake")

  })
  .on('error', console.error);

}

async function withdrawStake(pubKey) {

  if (pubKey == undefined)
    {console.log("Please call with correct params"); return}

  var staking = await getContract(pubKey, "staking");

  await staking.methods.withdraw()
  .send({from: pubKey.toString(), gas: DEFAULT_GAS})
  .on('receipt', (stakeReceipt) => {

    console.log("You withdrew your stake")

  })
  .on('error', console.error);

}

async function getHostStartTime(host) {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var time

  time = await staking.methods.getStartTime(host).call({from: defaultAddress});

  return time

}

async function isHostStaked(host) {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var isStaked

  isStaked = await staking.methods.getIsCurrentlyStaked(host)
    .call({from: defaultAddress});

  return isStaked

}

async function getAmountStaked(host) {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var amount

  amount = await staking.methods.getAmountStaked(host).call({from: defaultAddress});

  return amount

}

async function getAcceptanceTillWithdraw() {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var acceptance

  acceptance = await staking.methods.getAcceptanceTillWithdraw()
    .call({from: defaultAddress});

  return acceptance

}

async function getWithdrawalTime(host) {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var time

  time = await staking.methods.getWithdrawalTime(host)
    .call({from: defaultAddress});

  return time

}

async function getStakeNeeded() {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var stake

  stake = await staking.methods.getStakeNeeded().call({from: defaultAddress});

  return stake

}

async function getTotalStake() {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var stake

  stake = await staking.methods.getTotalStake().call({from: defaultAddress});

  return stake

}

async function getProbabilityDivisor() {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var divisor

  divisor = await staking.methods.getProbabilityDivisor()
    .call({from: defaultAddress});

  return divisor

}

async function getMaxProbability() {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var prob

  prob = await staking.methods.getMaxProbability().call({from: defaultAddress});

  return prob

}

async function getMaxStake() {

  var staking = await getContract(DEFAULT_ADDRESS, "staking");

  var stake

  stake = await staking.methods.getMaxStake().call({from: defaultAddress});

  return stake

}

//AGENT MANAGEMENT

async function getAgentAutonomy(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var autonomy

  autonomy = await management.methods.getAgentAutonomy(creator, position)
    .call({from: defaultAddress});

  return autonomy

}

async function getMinimumShard() {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var shardSize

  shardSize = await management.methods.getMinimumShardSize()
    .call({from: defaultAddress});

  return shardSize

}

async function getNameAvailability(name) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var availability

  availability = await management.methods.isNameTaken(name)
    .call({from: defaultAddress});

  return availability

}

async function createAgent(pubKey, agentIsCreator, creatorAgentAddr, permissionPos, name, source, codeHash, newBotAddress) {

  if (name == undefined || pubKey == undefined || source == undefined || codeHash == undefined || newBotAddress == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "agentManagement");

  var availability

  management.methods.isNameTaken(name).call({from: pubKey.toString()}).then(async (res) => {

    availability = res

    if (availability == undefined) {console.log("Error in checking name availability. Abort"); return}
    else if (availability == true) {console.log("The agent with this name has already been taken"); return}

    await management.methods.newAgent(agentIsCreator, creatorAgentAddr, permissionPos,
      name, source, codeHash, newBotAddress)
      .send({from: pubKey.toString(), gas: DEFAULT_GAS})
      .on('receipt', (stakeReceipt) => {

        console.log("You created a new agent called " + name)

      })
      .on('error', console.error);

  })

}

async function makeAutonomous(pubKey, name, position) {

  if (position == undefined || pubKey == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "agentManagement");

  await management.methods.makeFullyAutonomous(name, position)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("You made " + name + " fully autonomous")

    })
    .on('error', console.error);

}

async function updateAgentCode(pubKey, name, permissionPos, codeSource, codeHash) {

  if (position == undefined || pubKey == undefined || creator == undefined
      || codeSource == undefined || codeHash == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "agentManagement");

  await management.methods.updateCode(name, codeSource, codeHash)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("Successfuly updated " + name + "'s code")

    })
    .on('error', console.error);

}

async function setTreasury(pubKey, treasury) {

  if (pubKey == undefined || treasury == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "agentManagement");

  await management.methods.setTreasury(treasury)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("The treasury address was updated in agent management")

    })
    .on('error', console.error);

}

async function changeMinimumShardSize(pubKey, shardSize) {

  if (pubKey == undefined || shardSize == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "agentManagement");

  await management.methods.changeMinimumShardSize(shardSize)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("The minimum shard size was updated to " + shardSize)

    })
    .on('error', console.error);

}

async function deleteAgent(pubKey, name) {

  if (pubKey == undefined || name == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "agentManagement");

  await management.methods.deleteAgent(name)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("You successfuly deleted the agent named " + name)

    })
    .on('error', console.error);

}

async function getCreatorFromName(name) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var creator

  creator = await management.methods.getCreatorFromName(name)
    .call({from: defaultAddress});

  return creator

}

async function getPositionFromName(name) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var position

  position = await management.methods.getPositionFromName(name)
    .call({from: defaultAddress})

  return position

}

async function getAgentName(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var name

  name = await management.methods.getAgentName(creator, position)
    .call({from: defaultAddress})

  return name

}

async function getAgentControllingAgent(controlledAgent) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var master

  master = await management.methods.getAgentControllingAgent(controlledAgent)
    .call({from: defaultAddress})

  return master

}

async function getCreatorIsAgent(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var creator

  creator = await management.methods.getCreatorIsAgent(creator, position)
    .call({from: defaultAddress})

  return creator

}

async function getAgentVersion(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var version

  version = await management.methods.getAgentVersion(creator, position)
    .call({from: defaultAddress})

  return version

}

async function getAgentSource(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var source

  source = await management.methods.getAgentSourceLocation(creator, position)
    .call({from: defaultAddress})

  return source

}

async function getAgentCodeHash(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var codeHash

  codeHash = await management.methods.getAgentCodeHash(creator, position)
    .call({from: defaultAddress})

  return codeHash

}

async function getAgentAutonomoy(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var autonomy

  autonomy = await management.methods.getAgentAutonomoy(creator, position)
    .call({from: defaultAddress})

  return autonomy

}

async function getAgentAddress(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var addr

  addr = await management.methods.getAgentAddress(creator, position)
    .call({from: defaultAddress})

  return addr

}

async function getCurrentController(creator, position) {

  var management = await getContract(DEFAULT_ADDRESS, "agentManagement");

  var controller

  controller = await management.methods.getCurrentController(creator, position)
    .call({from: defaultAddress})

  return controller

}

//HOST MANAGEMENT

async function changeAgentsNumber(host, agentsNr, pubKey) {

  var management = await getContract(pubKey, "hostManagement");

  await management.methods.setAgentsNumber(host, agentsNr)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("Successfully changed the number of agents " + host + " has")

    })
    .on('error', console.error);

}

async function registerSeconds(pubKey, host, agent, nrSeconds) {

  var management = await getContract(pubKey, "hostManagement");

  await management.methods.registerSeconds(host, agent, nrSeconds)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("Successfully logged " + nrSeconds.toString() + "seconds of being online")

    })
    .on('error', console.error);

}

async function hostCanInteract(hostP2PId) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var canHost

  canHost = await management.methods.canHostAgents(hostP2PId).call({from: defaultAddress})

  return canHost

}

async function priceFromAddress(hostP2PId) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var price

  price = await management.methods.priceFromAddress(hostP2PId).call({from: defaultAddress})

  return price

}

async function getSecondsHosted(hostAddress, agent) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var secondsHosted

  secondsHosted = await management.methods.getSecondsHosted(hostAddress, agent).call({from: defaultAddress})

  return secondsHosted

}

async function stringToBytes32(someStr) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var bytes

  bytes = await management.methods.stringToBytes32(someStr)
    .call({from: defaultAddress})

  return bytes

}

async function getStartOfWork(hostAddress) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var start

  start = await management.methods.getStartOfWork(hostAddress).call({from: defaultAddress})

  return start;

}

async function getShutdownPermission(hostAddress) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var shutdownTime

  await management.methods.getShutdownPermission(hostAddress).call(function(err, result) {

    if (err) {shutdownTime = undefined; console.log(err)}

    else shutdownTime = result

  });

  return shutdownTime

}

async function getHostRent(hostP2PId) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var price

  price = await management.methods.hostPrice(hostP2PId).call({from: defaultAddress})

  return price;

}

async function getHostAddrFromID(hostP2PId) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var address

  address = await management.methods.hostAddressfromID(hostP2PId).call()

  return address;

}

async function getHostIDFromAddr(hostAddr) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var id

  id = await management.methods.hostIDfromAddr(hostAddr).call({from: defaultAddress})

  return id;

}

async function getTimeBetweenPriceChanges() {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var time

  time = await management.methods.getTimeBetweenChanges().call({from: defaultAddress})

  return time

}

async function getAgentsCount(hostID) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var count

  count = management.methods.getAgentsNumber(hostID).call({from: defaultAddress})

  return count

}

async function getMinimumShutdownTime() {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var time

  time = await management.methods.getMinimumShutdownTime().call({from: defaultAddress})

  return time

}

async function getAgentsTree(host) {

  var management = await getContract(DEFAULT_ADDRESS, "hostManagement");

  var tree

  tree = await management.methods.getAgentsTree(host).call({from: defaultAddress})

  return tree

}

async function changeRent(rent, pubKey) {

  if (rent == undefined || rent <= 0 || pubKey == undefined) {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "hostManagement");

  management.methods.changeRent(rent)
  .send({from: pubKey.toString(), gas: DEFAULT_GAS})
  .on('receipt', (stakeReceipt) => {

    console.log("You've successfully changed your rent to " + rent)

  })
  .on('error', console.error);

}

async function setHostID(id, pubKey) {

  if (id == undefined || pubKey == undefined) {

    console.log("Please call with correct params")

    return

  }

  var management = await getContract(pubKey, "hostManagement");

  management.methods.setID(id).send({from: pubKey.toString(), gas: DEFAULT_GAS})
  .on('receipt', (stakeReceipt) => {

    console.log("You've successfully set your ID to " + id)

  })
  .on('error', console.error);

}

async function setAgentsTree(pubKey, host, ipfs, root, agentsNr) {

  if (pubKey == undefined || host == undefined || ipfs == undefined || root == undefined || agentsNr == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "hostManagement");

  management.methods.setAgentsTree(host, ipfs, root, agentsNr)
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

    })
    .on('error', console.error);

}

async function broadcastActivity(pubKey) {

  if (pubKey == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "hostManagement");

  management.methods.broadcastActivity()
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("You've successfully broadcasted your availability" + "\n")

    })
    .on('error', console.error);

}

async function shutdown(pubKey) {

  if (pubKey == undefined)
    {console.log("Please call with correct params"); return}

  var management = await getContract(pubKey, "hostManagement");

  management.methods.shutdown()
    .send({from: pubKey.toString(), gas: DEFAULT_GAS})
    .on('receipt', (stakeReceipt) => {

      console.log("You've successfully shutdown your node")

    })
    .on('error', console.error);

}

module.exports = {

    web3,
    readFile,

    proposePermission,
    usePermission,
    canUsePermission,
    getLatestNonce,
    getContractWhereUsed,
    getFunctionWhereUsed,
    getHashOfParams,
    getTrustedExecutionProof,
    getCreationTime,
    getUsed,
    getHost,
    getAvailability,
    getContractAllowance,

    changingAgentTree,
    finishedChangingTree,
    getChangingTree,
    getIsChangingTree,
    getHostChangingTree,

    getMinimumShard,
    getNameAvailability,
    hostCanInteract,
    getHostRent,
    getHostAddrFromID,
    getTimeBetweenPriceChanges,
    getMinimumShutdownTime,
    stake,
    changeRent,
    setHostID,
    createAgent,
    makeAutonomous,
    updateAgentCode,
    getAgentName,
    getAgentVersion,
    getAgentSource,
    getAgentCodeHash,
    getAgentAutonomoy,
    getAgentAddress,
    getCurrentController,

    setTreasury,
    changeMinimumShardSize,
    signalStakeWithdrawal,
    withdrawStake,
    getHostStartTime,
    isHostStaked,
    getAmountStaked,
    getAcceptanceTillWithdraw,
    getWithdrawalTime,
    getStakeNeeded,
    getTotalStake,
    getProbabilityDivisor,
    getMaxProbability,
    getMaxStake,

    getCreatorFromName,
    getPositionFromName,
    priceFromAddress,
    deleteAgent,
    getWithdrawalData,
    getDeploymentHostsTree,
    getDeploymentIntData,
    getShardModifications,
    getWithdrawalIPFSProof,
    getWithdrawalRoot,
    getWithdrawalsLength,
    getDeploymentBillingProof,
    getDeploymentBillingRoot,
    getDeploymentHostsRoot,
    getDeploymentIPFSProof,
    getDeploymentsLength,
    hasEnoughResponses,
    getAgentManagementAddr,
    respondToRequest,
    withdrawAgent,
    deployAgent,
    changeAgentManagement,
    changeStakeAddress,
    setShardModifications,
    agentVoted,
    getModificationIntData,
    getVotingWindow,
    getVoteThreshold,
    getBatchSize,
    applyModification,
    voteModification,
    proposeModification,
    changeVoteThreshold,
    changeBatchSize,
    changeShardAgentManagement,
    changeVotingWindow,
    setShardDeploymentReq,
    setShardTreasury,
    getContribution,
    agentIsRegistered,
    getAgentRoot,
    getAgentIntData,
    agentAlreadyRegistered,
    getPaymentWindow,
    getAgentManagementTreasury,
    getPaid,
    withdrawFunds,
    fundAgent,
    applyModificationTr,
    registerAgentTr,
    changeTreasuryAgentManagement,
    changePaymentWindow,
    setTreasuryDeploymentRequests,
    setTreasuryShardModifications,
    getHostIDFromAddr,
    getAgentsCount,
    getContract,
    getCurrentBlockNr,
    getModificationTree,
    getModificationArrays,
    setAgentsTree,
    broadcastActivity,
    shutdown,
    getAgentsTree,
    registerSeconds,
    getSecondsHosted,
    stringToBytes32,
    getStartOfWork,
    setHostManagement,
    getSecondsBilled,
    getHostAgentVersion,
    clearAgent,
    updateAgentVersion,
    getAgentAutonomy,
    getAgentControllingAgent,
    getCreatorIsAgent,
    getSecondsToBill

}
