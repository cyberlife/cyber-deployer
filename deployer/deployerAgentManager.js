var sha256 = require('js-sha256')
var chain = require('../chain/chainInteraction')
const BigNumber = require('bignumber.js');
var ethUtils = require('ethereumjs-util')
var sha3 = require('ethereumjs-util').sha3
const MerkleTree = require('merkletreejs')
const crypto = require('crypto')
const async = require('async')

var chainCalls = require('../general/deployerChainCalls')
var networkHelpers = require('../general/networkHelpers')
var sorting = require('../general/sorting')
var utils = require('../general/utils')

var ipfsUtils = require('../general/ipfsInteraction')
const IPFS = require('ipfs-api');
const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });

var web3 = chain.web3
var pingDelay = 10000

async function withdrawAgent(personalNode, protocolName, pubKey, peerList, withdrawalData) {

  var i = 0

  var withdrawal = JSON.parse(withdrawalData)

  var registered = await chain.agentIsRegistered(withdrawal.name)

  if (!registered) {console.log(withdrawal.name + ' is not a registered agent'); return}

  var agentRootData = await chain.getAgentRoot(withdrawal.name)

  var minimumShard = await chain.getMinimumShard()

  var availableWithdrawals = agentRootData[3] - minimumShard

  if (availableWithdrawals < withdrawal.hostsNumber) {

    console.log("You want to withdraw from too many hosts. The maximum allowed number right now is " + availableWithdrawals.toString())

    return

  }

  console.log("Deleting hosts that charge the most...")

  await ipfsUtils.getIPFSData(agentRootData[2], async function(err, billingData) {

    var billings = billingData[0].content.toString('utf8').split('\n')

    var decomposedBillings = await utils.decomposeBillings(billings)

    var currentTime = new BigNumber(Math.floor(Date.now() / 1000))

    var remainingPayments = []

    for (i = 0; i < decomposedBillings[1].length; i++) {

      remainingPayments.push(new BigNumber( (decomposedBillings[3][i] - currentTime)
          * decomposedBillings[2][i] ) )

    }

    var sortedHosts = await sorting.quickSort(remainingPayments, [decomposedBillings[0]],
                            0, decomposedBillings[0].length - 1, 1)

    var sortedHLength = sortedHosts[1][0].length

    var hostsToDelete = sortedHosts[1][0]
        .slice(sortedHLength - withdrawal.hostsNumber, sortedHLength)

    var remainingLeaves = await networkHelpers.getWithdrawalLeaves(hostsToDelete, billings)

    var hashedWithdrawalLeaves = await utils.getHashedLeaves(remainingLeaves[0])

    var bufferedWithdrawalLeaves = await utils.stringArrToBuffArr(remainingLeaves[0])

    const billingsTree = new MerkleTree(hashedWithdrawalLeaves, sha3)

    var billingsRoot = await billingsTree.getRoot()

    var billingsIPFS = await ipfsUtils.createIPFSData(bufferedWithdrawalLeaves)

    var hostsAddresses = await utils.idsToAddresses(hostsToDelete)

    console.log("Storing withdrawal merkle data...")

    ipfsUtils.postDatatoIPFS(billingsIPFS, async function(errBillings, billingsHash) {

      if (errBillings != null) {console.log(errBillings); return}

      if (billingsHash == NaN || billingsHash == undefined) {console.log("Could not store billing data"); return}

      var totalRemainingHosts = parseInt(agentRootData[3].toString()) - withdrawal.hostsNumber

      await chain.withdrawAgent(pubKey, withdrawal.name, billingsRoot,
          billingsHash[0].hash.toString(), totalRemainingHosts)

      await chain.proposeModification(pubKey, withdrawal.name, hostsAddresses,
                                      remainingLeaves[1], remainingLeaves[2],
                                      billingsRoot, billingsHash[0].hash.toString(),
                                      [1, totalRemainingHosts, 0])

      var modificationIntData = await chain.getModificationIntData(withdrawal.name)

      var voteWindow = await chain.getVotingWindow()

      var voteThreshold = await chain.getVoteThreshold()

      var hostsContacted = 0

      var requestPosition = await chain.getWithdrawalsLength(withdrawal.name)

      var dialData = await utils.createJSON(['type', 'name'], ['string', 'string'],
        ["shard_modif", withdrawal.name])

      var phoneBooks = await networkHelpers.getHostsPhoneBooks(peerList, hostsToDelete)

      //Contact peers and ask them to vote the modification

      console.log("Contacting hosts...")

      for (i = 0; i < peerList.length; i++) {

        var idToBytes = await chain.stringToBytes32(peerList[i].id.toB58String())

        var peerIsHost = await chain.hostCanInteract(idToBytes)

        if (peerIsHost == false) continue

        await networkHelpers.dialNode(personalNode, protocolName, peerList[i], [dialData])

      }

      console.log('Waiting for votes on the modification...')

      applyShard(pubKey, personalNode, protocolName, withdrawal.name,
                 new BigNumber(modificationIntData[0]).plus(voteWindow),
                 voteThreshold, phoneBooks, hostsToDelete, 0)

    })

  })

}

async function deployAgent(personalNode, protocolName, pubKey, privateKey, peerList, _deploymentData) {

  if (privateKey == undefined) {

    console.log("You did not provide a valid private key\n")

    return

  }

  var deploymentData = JSON.parse(_deploymentData)

  //Check if this deployment is a registration in treasury or a shard modification and check responsesNumber correctness

  var agentIsInTreasury = await chain.agentAlreadyRegistered(deploymentData.name)

  if (agentIsInTreasury == true) {

    var batchSize = await chain.getBatchSize()

    if (batchSize == undefined) {console.log("Could not get batch size from shard modifications"); return}

    if (deploymentData.responsesNumber > batchSize) {

      console.log("Responses number needs to be at maximum " + batchSize.toString())

      return

    }

  } else if (agentIsInTreasury == false) {

    var minimumShard = await chain.getMinimumShard()

    if (minimumShard != undefined && minimumShard > deploymentData.responsesNumber)
      {console.log("Your shard size needs to be at least " + minimumShard); return}

    else if (minimumShard == undefined) {console.log("Could not retreive the shard size. Abort"); return}

  } else {console.log("Could not check if agent is in treasury"); return}

  var creator = await chain.getCreatorFromName(deploymentData.name)

  if (creator == undefined || creator.toLowerCase() != pubKey) {

    console.log("You can't change the shard of this agent. The agent is either not registered or you are not the creator")

    return

  }

  var position = await chain.getPositionFromName(deploymentData.name)

  var aut = await chain.getAgentAutonomy(creator.toString(), parseInt(position))

  if (aut == undefined) {console.log("Could not check if agent is autonomous. Abort"); return}

  if (aut == true) {

    console.log("The agent is autonomous, you can't change its shard anymore")

    return

  }

  await chainCalls.getCurrentAgentShard(deploymentData.name, async function(shardErr, shard) {

    var stakedHosts = []

    stakedHosts = await networkHelpers.getHostsWhichInteract(peerList)

    if (shard != undefined && shard.length > 0) {

      stakedHosts = await networkHelpers.ignoreOccupiedHosts(shard, stakedHosts)

    }

    if (stakedHosts.length < deploymentData.responsesNumber) {

        console.log("Not enough hosts you connected to are able to receive agents. Wait for more connections");
        return

    }

    //Get data for each host

    var hostPrices = await chainCalls.getHostPrices(stakedHosts)

    //Sort according to rents

    var rentSorted = await sorting.quickSort(hostPrices, [stakedHosts], 0, hostPrices.length - 1, 2)

    //Get money needed to deploy and check personal balance

    var requestData = await networkHelpers.getRequestData(rentSorted, deploymentData.responsesNumber, deploymentData.time, 60)

    var personalBalance = await web3.eth.getBalance(pubKey)

    if (personalBalance < requestData[0]) {

      console.log("You don't have enough money to deploy the agent on the current connected hosts for "
        + deploymentData.time + " seconds. Please reduce the time or connect to other hosts")

      return

    }

    //Copy requests[2] because, in case of shard modification, we need the info from the new hosts

    var shardModifData = requestData[2].concat()

    //Create leaves for merkle trees

    var hostsLeaves = await utils.getHostsLeaves(requestData[1])

    var billingLeaves = await utils.getHashedLeaves(requestData[2])

    //Add pre-existing leaves if these exist

    var currentLeaves

    if (shard != undefined && shard.length > 0) {

      currentLeaves = await utils.getCurrentShardLeaves(shard)

    }

    if (currentLeaves != undefined && currentLeaves.constructor === Array && currentLeaves.length > 0) {

      var auxCurrentLeaves = currentLeaves.concat()

      auxCurrentLeaves[0] = await utils.getHashedLeaves(auxCurrentLeaves[0])
      auxCurrentLeaves[1] = await utils.getHashedLeaves(auxCurrentLeaves[1])

      requestData[2] = currentLeaves[1].concat(requestData[2])

      hostsLeaves = auxCurrentLeaves[0].concat(hostsLeaves)
      billingLeaves = auxCurrentLeaves[1].concat(billingLeaves)

    }

    //Create merkle trees and get roots

    const hostsTree = new MerkleTree(hostsLeaves, sha3)

    var hostsRoot = await hostsTree.getRoot()

    const billingTree = new MerkleTree(billingLeaves, sha3)

    var billingRoot = await billingTree.getRoot()

    //Post IDs and billings to IPFS

    var hostsIPFS = await ipfsUtils.createIPFSData(hostsLeaves)

    var bufferedBillings = await utils.stringArrToBuffArr(requestData[2])

    var billingsIPFS = await ipfsUtils.createIPFSData(bufferedBillings)

    console.log("Storing deployment merkle data...")

    ipfsUtils.postDatatoIPFS(hostsIPFS, function(errHosts, hostsHash) {

      if (errHosts) {console.log(errHosts); return}

      if (hostsHash == NaN || hostsHash == undefined) {console.log("Could not store hosts data"); return}

      ipfsUtils.postDatatoIPFS(billingsIPFS, async function(errBillings, billingsHash) {

        if (errBillings) {console.log(errBillings); return}

        if (billingsHash == NaN || billingsHash == undefined) {console.log("Could not store billing data"); return}

        //Call deployment function from SC

        await chain.deployAgent( pubKey, deploymentData.name, hostsRoot,
          hostsHash[0].hash.toString(), billingRoot, billingsHash[0].hash.toString(),
          deploymentData.responsesNumber, requestData[0].toString() )

        //Contact hosts and ask them to respond to request

        var requestPosition = await chain.getDeploymentsLength(deploymentData.name)
        var dialResponse = 0

        var dialData = await utils.createJSON(['type', 'name', 'position'], ['string', 'string', 'int'],
          ["deployment_response", deploymentData.name, requestPosition - 1])

        console.log("Contacting hosts...")

        for (var i = 0; i < requestData[1].length; i++) {

          dialResponse = networkHelpers.dialNode(personalNode, protocolName, requestData[1][i], [dialData])

          if (dialResponse != undefined && dialResponse != NaN && dialResponse == -1) {

            console.log("Could not contact a host. Aborting deployment")
            break

          }

        }

        var requestIntData = await chain.getDeploymentIntData(deploymentData.name, requestPosition - 1)

        checkRequestConfirmations(pubKey, personalNode, protocolName,
                                  deploymentData.name, requestPosition - 1, requestIntData[1],
                                  billingRoot, billingsHash[0].hash.toString(), shardModifData,
                                  requestData[2].length, requestData[1], requestData[0])

        console.log("Listening for confirmations...")

      })

    })

  })

}

async function checkRequestConfirmations(pubKey, personalNode, protocolName,
                                         name, position, requestCreationTime,
                                         billingRoot, billingIPFS, billingsArr,
                                         totalHostsNr, hostsPeerIDs, money) {

  var enoughResponses = await chain.hasEnoughResponses(name, position)

  if (enoughResponses == true) {

    var agentRegistered = await chain.agentAlreadyRegistered(name)

    if (agentRegistered == false) {

      //Mark the agent as being installed

      var isBeingMigrated = await chain.getChangingTree(name)

      if (isBeingMigrated) {

        console.log("The agent is being migrated by other entities. Please try to deploy it later");

        return

      } else {await chain.changingAgentTree(pubKey, name)}

      //Add the agent in each host's tree

      await chainCalls.changeHostsTrees(pubKey, name, hostsPeerIDs, 0)

      //Register the agent

      await chain.registerAgentTr(pubKey, name, billingRoot, billingIPFS, totalHostsNr, money)

      //Send signal to install the agent

      var hostInstruction = await utils.createJSON(['type', 'name'], ['string', 'string'],
        ["install_agent", name])

      for (var i = 0; i < hostsPeerIDs.length; i++) {

        networkHelpers.dialNode(personalNode, protocolName, hostsPeerIDs[i], [hostInstruction])

      }

      await chain.finishedChangingTree(pubKey, name)

    } else if (agentRegistered == true) {

      modifyShard(pubKey, personalNode, protocolName, name, position, 0, billingRoot, billingIPFS,
          billingsArr, totalHostsNr, hostsPeerIDs, money.toString())

    } else {

      console.log("Got enough responses but could not check the status of the agent\n")

      return

    }

  } else {

    var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

    //TODO: change 60 and get value from SC

    var responsesWindow = new BigNumber(60)

    if (currentDate.minus(requestCreationTime).isLessThan(responsesWindow)) {

      setTimeout(checkRequestConfirmations, pingDelay, pubKey, personalNode, protocolName, name, position,
          requestCreationTime, billingRoot, billingIPFS, billingsArr, totalHostsNr, hostsPeerIDs, money)

    } else {

      console.log("Failed request. Did not get enough responses\n")

    }

  }

}

async function modifyShard(pubKey, personalNode, protocolName, name, position, modifType, billingRoot, billingIPFS,
    billingsArr, totalHostsNr, hostsPeerIDs, money) {

  console.log('Sending a new shard modification...')

  var decomposedBillings = await utils.decomposeBillings(billingsArr)

  var hostsAddrs = await utils.idsToAddresses(decomposedBillings[0])

  await chain.proposeModification(pubKey, name, hostsAddrs, decomposedBillings[1], decomposedBillings[2],
                                  billingRoot, billingIPFS, [modifType, totalHostsNr, money])

  var modificationIntData = await chain.getModificationIntData(name)

  var voteWindow = await chain.getVotingWindow()

  var voteThreshold = await chain.getVoteThreshold()

  var dialData = await utils.createJSON(['type', 'name'], ['string', 'string'],
    ["shard_modif", name])

  var dialResponse = 1

  for (var i = 0; i < hostsPeerIDs.length; i++) {

    var idToBytes = await chain.stringToBytes32(hostsPeerIDs[i].id.toB58String())

    var peerIsHost = await chain.hostCanInteract(idToBytes)

    if (peerIsHost == false) continue

    await networkHelpers.dialNode(personalNode, protocolName, hostsPeerIDs[i], [dialData])

  }

  console.log('Waiting for votes on the modification...')

  applyShard(pubKey, personalNode, protocolName, name,
      new BigNumber(modificationIntData[0]).plus(voteWindow), voteThreshold, hostsPeerIDs, undefined, money)

}

async function applyShard(pubKey, personalNode, protocolName, name, untilWhen,
    threshold, hostsPeerIDs, hostsToDelete, money) {

  var hostInstruction, i

  var modifIntData = await chain.getModificationIntData(name)

  if (modifIntData[3] >= threshold) {

    //Mark the agent as being installed

    var isBeingMigrated = await chain.getChangingTree(name)

    if (isBeingMigrated) {

      console.log("The agent is being migrated by other entities. Please try to deploy it later");

      return

    } else await chain.changingAgentTree(pubKey, name)

    if (modifIntData[1] == 0) {

      await chainCalls.changeHostsTrees(pubKey, name, hostsPeerIDs, 0)

      await chain.applyModification(pubKey, name, money)

      //Send signal to install the agent

      hostInstruction = await utils.createJSON(['type', 'name'], ['string', 'string'],
        ["install_agent", name])

      for (i = 0; i < hostsPeerIDs.length; i++) {

        networkHelpers.dialNode(personalNode, protocolName, hostsPeerIDs[i], [hostInstruction])

      }

      await chain.finishedChangingTree(pubKey, name)

    } else if (modifIntData[1] == 1) {

      //Delete the agent from each host's tree

      await chainCalls.changeHostsTrees(pubKey, name, hostsToDelete, 1)

      await chain.applyModification(pubKey, name, 0)

      hostInstruction = await utils.createJSON(['type', 'name'], ['string', 'string'],
        ["uninstall_agent", name])

      for (i = 0; i < hostsPeerIDs.length; i++) {

        networkHelpers.dialNode(personalNode, protocolName, hostsPeerIDs[i], [hostInstruction])

      }

      await chain.finishedChangingTree(pubKey, name)

    }

  } else {

    var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

    if (currentDate < untilWhen) {

      setTimeout(applyShard, pingDelay, pubKey, personalNode, protocolName, name,
          untilWhen, threshold, hostsPeerIDs, hostsToDelete, money)

    } else {

      console.log("The shard modification for " + name + " did not get enough acceptance votes" + "\n")

    }

  }

}

module.exports = {

  deployAgent,
  checkRequestConfirmations,
  modifyShard,
  applyShard,
  withdrawAgent

}
